<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Barang
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/admin', 'HomeController@adminHome')->name('admin.home');



// //RumahSakit
// Route::get('rumahsakit', function () {
//     return view('rumahsakit/index');
// });
// Route::resource('rumahsakit','RumahSakitController');

// //Others
// Route::get('/spesialis', 'SpesialisController@index');
// Route::get('/spesialis/create', 'SpesialisController@create');
// Route::post('/spesialis', 'SpesialisController@store');
// Route::get('/spesialis/{spesialis}/edit','SpesialisController@edit');
// Route::patch('/spesialis/{spesialis}', 'SpesialisController@update');
// Route::delete('/spesialis/{spesialis}', 'SpesialisController@destroy');


// jadwal
Route::get('/jadwal', 'JadwalController@index')->name('getJadwal');
Route::post('/jadwal', 'JadwalController@simpan_jadwal')->name('simpan_jadwal');
Route::get('/jadwal/{JADWAL_ID}', 'JadwalController@hapus_jadwal');
// Route::post('/jadwal', 'JadwalController@store');
// Route::get('/jadwal/{jadwal}/edit','JadwalController@edit');
// Route::patch('/jadwal/{jadwal}', 'JadwalController@update');
// Route::delete('/jadwal/{jadwal}', 'JadwalController@destroy');

// kota
Route::get('/kota', 'KotaController@index')->name('getKota');
Route::post('/kota', 'KotaController@simpan_kota')->name('simpan_kota');
Route::get('/kota/{id}', 'KotaController@hapus_kota');

// History
Route::get('/history', 'HistoryController@index')->name('getHistory');
Route::get('/history/{id}', 'HistoryController@hapus_history');

// rumahsakit
Route::get('/rumahsakit', 'RumahSakitController@index')->name('getRumahsakit');
Route::post('/rumahsakit', 'RumahSakitController@simpan_rumahsakit')->name('simpan_rumahsakit');
Route::get('/rumahsakit/{id}', 'RumahSakitController@hapus_rumahsakit');

// spesialis
Route::get('/spesialis', 'SpesialisController@index')->name('getSpesialis');
Route::post('/spesialis', 'SpesialisController@simpan_spesialis')->name('simpan_spesialis');
Route::get('/spesialis/{idsp}', 'SpesialisController@hapus_spesialis');

// Dokter
Route::get('/dokter', 'DokterController@index')->name('getDokter');
Route::post('/dokter', 'DokterController@simpan_dokter')->name('simpan_dokter');
Route::get('/dokter/{DOKTER_ID}', 'DokterController@hapus_dokter');
// Route::get('/rumahsakit', 'RumahSakitController@index');
// Route::get('/rumahsakit/create', 'RumahSakitController@create');
// Route::get('/rumahsakit/{rumahsakit}/edit','RumahSakitController@edit');
// Route::patch('/rumahsakit/{rumahsakit}', 'RumahSakitController@update');
// Route::delete('/rumahsakit/{rumahsakit}', 'RumahSakitController@destroy');

// Route::resource('dokter','DokterController');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/RS/getData', 'DataController@getDataRS')->name('getDataRS');
Route::post('/dokter/getDataDokter', 'DataController@getDataDokter')->name('getDataDokter');
Route::post('/getNamaSpesialis', 'DataController@getNamaSpesialis')->name('getNamaSpesialis');
Route::get('/dokter/getDataDokterSp', 'DataController@getDataDokterSp')->name('getDataDokterSp');
Route::post('/dokter/getDataDokterLengkap', 'DataController@getDataDokterLengkap')->name('getDataDokterLengkap');
Route::post('/dokter/getJadwalDokter', 'DataController@getJadwalDokter')->name('getJadwalDokter');
// Route::post('/cekToken', 'DataController@cek_CRSF')->name('getJadwalDokter');

// Route::post('/cobaCreate', 'DataController@cobaCreate');
// Route::get('spesialis/{id}/Edit', 'DataController@cobaEdit');
// Route::patch('spesialis/cobaUpdate/{id}', 'DataController@cobaUpdate');
// Route::delete('spesialis/cobaDelete/{id}', 'DataController@cobaDelete');

//=============== Grub User ADMIN =============== //

Route::group(['namespace' => 'Admin', 'middleware' => ['auth_admin'], 'prefix' => 'admin'], function () {

    Route::get('landing', 'adminController@index');
});

//=============== Grub User DOKTER =============== //
Route::group(['namespace' => 'Dokter', 'middleware' => ['auth_dokter'], 'prefix' => 'dokter'], function () {

    Route::get('landing', 'DokterController@index');
});

//=============== Grub User RUMAH SAKIT =============== //
Route::group(['namespace' => 'RumahSakit', 'middleware' => ['auth_rumahsakit'], 'prefix' => 'rumahsakit'], function () {

    Route::get('landing', 'RumahSakitController@index');
});

//=============== Grub User SPESIALIS =============== //
Route::group(['namespace' => 'Spesialis', 'middleware' => ['auth_spesialis'], 'prefix' => 'spesialis'], function () {

    Route::get('landing', 'SpesialisController@index');
});

//=============== Grub User USER =============== //
Route::group(['namespace' => 'User', 'middleware' => ['auth_user'], 'prefix' => 'user'], function () {

    Route::get('landing', 'userController@index');
});
