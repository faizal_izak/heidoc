@extends('layouts.dasboardadminadd')

@section('sidebar')
  <!-- Sidebar -->
  <div class="sidebar" >                  
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">                
        <li class="nav-item menu-open">
          <a href="{{url('admin')}}" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/rumahsakit') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Rumah Sakit</p>
          </a>               
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/spesialis') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Spesialis</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/dokter') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Dokter</p>
          </a>                
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/jadwal') }}" class="nav-link active">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Jadwal</p>
          </a>                
        </li> 
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Halaman Admin</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/jadwal') }}">Data Jadwal</a></li>
                        <li class="breadcrumb-item active">Tambah Data Jadwal</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Jadwal</h3>
                        </div>
                        <div class="card-body">                                                      
                            <form method="post" action="{{ url('/jadwal') }}">     
                                @csrf                                    
                                
                                <div class="form-group row">
                                    <label for="DOKTER_ID" class="col-sm-2 col-form-label">Nama Dokter</label>
                                    <div class="col-sm-4">
                                        <select id="DOKTER_ID" class="form-control @error('DOKTER_ID')
                                        is-invalid @enderror" name="DOKTER_ID">                                          
                                            @foreach($dokter as $list)
                                                <option value="{{$list->id}}"> {{$list->DOKTER_NAMA}} </option>
                                            @endforeach
                                        </select>
                                        @error('DOKTER_ID')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="JADWAL_HARI" class="col-sm-2 col-form-label">Hari</label>
                                    <div class="col-sm-4">
                                        <select id="JADWAL_HARI" class="form-control @error('JADWAL_HARI')
                                        is-invalid @enderror" name="JADWAL_HARI">                                          
                                            @foreach($hari as $list)
                                                <option value="{{$list}}"> {{$list}} </option>
                                            @endforeach
                                        </select>
                                        @error('JADWAL_HARI')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="JADWAL_WAKTU" class="col-sm-2 col-form-label">Waktu</label>
                                    <div class="col-sm-4">
                                        <select id="JADWAL_WAKTU" class="form-control @error('JADWAL_WAKTU')
                                        is-invalid @enderror" name="JADWAL_WAKTU">                                          
                                            @foreach($waktu as $list)
                                                <option value="{{$list}}"> {{$list}} </option>
                                            @endforeach
                                        </select>
                                        @error('JADWAL_WAKTU')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="JADWAL_JAM_M" class="col-sm-2 col-form-label">Jam Mulai</label>
                                    <div class="col-sm-4">
                                        <input type="time" class="form-control @error('JADWAL_JAM_M')
                                        is-invalid @enderror" id="JADWAL_JAM_M" placeholder=""
                                        name="JADWAL_JAM_M" value="{{ old('JADWAL_JAM_M') }}">
                                        @error('JADWAL_JAM_M')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="JADWAL_JAM_S" class="col-sm-2 col-form-label">Jam Selesai</label>
                                    <div class="col-sm-4">
                                        <input type="time" class="form-control @error('JADWAL_JAM_S')
                                        is-invalid @enderror" id="JADWAL_JAM_S" placeholder=""
                                        name="JADWAL_JAM_S" value="{{ old('JADWAL_JAM_S') }}">
                                        @error('JADWAL_JAM_S')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="text-right">
                                <button type="submit" class="btn btn-primary">Tambah</button>    
                                <a href="{{ url('/jadwal') }}" class="btn btn-secondary">Kembali</a>                                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection