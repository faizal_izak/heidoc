@extends('layouts.app')
@php
$data_session = Session::get('user_app');
$token = $data_session['token'];
$namaUser = $data_session['userid'];
$urlEdit = url()->current();
@endphp
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Tabel Jadwal</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Tabel Jadwal</div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    {{$token}}
                    <h4><i class="fas fa-calendar-check"></i> Tabel Jadwal</h4>
                </div>
                <button class="btn btn-primary" id="btnTambah">Tambah Data</button>
                <div class="card-body">
                    <table id="table_jadwal" class="display table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="5%">No</th>
                                <th scope="col" width="20%">Nama Dokter</th>
                                <th scope="col" width="20%">Tanggal</th>
                                <th scope="col" width="20%">Jam Mulai</th>
                                <th scope="col" width="20%">Jam Selesai</th>
                                <th scope="col" width="30%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade modal-slide-from-bottom" id="modaljadwal" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="title" class="modal-title"></h5>
            </div>
            <div class="modal-body pb-0">
                <form method="post" id="formjadwal" name="formjadwal" action="{{route('simpan_jadwal')}}">
                    @csrf
                    <input type="hidden" name="action" value="">
                    <input type="hidden" name="JADWAL_ID" value="">
                    <div class="form-group">
                        <label>Nama Kota</label>
                        <select name="DOKTER_ID" id="DOKTER_ID" class="form-control">
                            @foreach ($optDokter as $key => $optdok)
                            <option value="{{$optdok['DOKTER_ID']}}">{{$optdok['DOKTER_NAMA']}}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-0">
                        <label>Tanggal</label>
                        <input type="date" name="JADWAL_TGL" id="JADWAL_TGL" class="form-control">
                    </div>
                    <div class="form-group mb-0">
                        <label>Jam Mulai</label>
                        <input type="time" name="JADWAL_JAM_M" id="JADWAL_JAM_M" class="form-control">
                    </div>
                    <div class="form-group mb-0">
                        <label>Jam Selesai</label>
                        <input type="time" name="JADWAL_JAM_S" id="JADWAL_JAM_S" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endModal -->
<script src="{{asset('assets/js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    var table = $('#table_jadwal').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "400px",
        "scrollCollapse": true,
        ajax: "{{ route('getJadwal') }}",
        columns: [{
                data: 'nomer',
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'NAMA_DOKTER',
                name: 'NAMA_DOKTER'
            },
            {
                data: 'JADWAL_TGL',
                name: 'JADWAL_TGL'
            },
            {
                data: 'JADWAL_JAM_M',
                name: 'JADWAL_JAM_M'
            },
            {
                data: 'JADWAL_JAM_S',
                name: 'JADWAL_JAM_S'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });
    // tambah
    $(document).ready(function() {
        $(document).on('click', '#btnTambah', function(e) {
            $('#modaljadwal').modal('show');
            $('#title').html('Tambah Jadwal');
            $("#formjadwal")[0].reset();
            $('input[name=action]').val('tambah');
        });
        // Edit
        $(document).on('click', '.btnEdit', function(e) {
            // console.log('.btnEdit');
            // alert('jjj');
            $('#title').html('Edit Jadwal');
            $('input[name=action]').val('edit');
            // console.log($(this).data('id'));
            var tanggal = $(this).data('tgl');
            var dokter = $(this).data('dokter');
            var tanggal_mulai = $(this).data('mulai');
            var tanggal_selesai = $(this).data('selesai');
            var id = $(this).data('id');
            $('input[name=JADWAL_ID]').val(id);
            console.log($('input[name=JADWAL_ID]').val(id));
            $('input[name=DOKTER_ID]').val(dokter);
            $('input[name=JADWAL_TGL]').val(tanggal);
            $('input[name=JADWAL_JAM_M]').val(tanggal_mulai);
            $('input[name=JADWAL_JAM_S]').val(tanggal_selesai);

            $('#modaljadwal').modal('show');
        });
        // delete
        $(document).on('click', '.delete', function(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yes, delete it!',
                        className: 'btn btn-success'

                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    console.log($(this).data('id'));
                    $.ajax({
                        url: '{{url()->current()}}/' + $(this).data('id'),
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            table.draw();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            //$('#modalOrg').modal('show');
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    });
</script>
@endsection