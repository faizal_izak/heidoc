@extends('layouts.app')
@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['userid'];

@endphp
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Tabel History</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Tabel History</div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <!-- {{$token}} -->

                    <h4><i class="fas fa-history"></i> Tabel History</h4>
                </div>
                <div class="card-body">
                    <!-- tabel -->
                    <table id="table_history" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Keluhan</th>
                                <th scope="col">Nama Dokter</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col" width="20%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                    <!-- endtabel -->
                </div>
            </div>
    </section>
</div>

<script src="{{asset('assets/js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    var table = $('#table_history').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        "scrollY": "350px",
        "scrollCollapse": true,
        ajax: "{{ route('getHistory') }}",
        columns: [{

                data: "nomer",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }

            },
            {
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'keluhan',
                name: 'keluhan'
            },
            {
                data: 'nama_dokter',
                name: 'nama_dokter'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });
    // tambah
    $(document).ready(function() {
        // delete
        $(document).on('click', '.delete', function(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yes, delete it!',
                        className: 'btn btn-success'

                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    console.log($(this).data('id'));
                    $.ajax({
                        url: '{{url()->current()}}/' + $(this).data('id'),
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            table.draw();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            //$('#modalOrg').modal('show');
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    });
</script>
@endsection