@extends('layouts.app')
@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['userid'];
$urlEdit = url()->current();
@endphp
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Tabel Dokter</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Tabel Dokter</div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <!-- {{$token}} -->
                    <h4><i class="fas fa-user-md"></i> Tabel Dokter</h4>
                </div>
                <button class="btn btn-primary" id="btnTambah">Tambah Data</button>
                <div class="card-body">
                    <!-- tabel -->
                    <table id="tbdokter" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col" width="5%">No</th>
                                <th scope="col" width="15%">Nama Rumah Sakit</th>
                                <th scope="col" width="25%">Nama Spesialis</th>
                                <th scope="col" width="25%">Nama Dokter</th>
                                <th scope="col" width="20%">HP</th>
                                <th scope="col" width="10%">NIP</th>
                                <th scope="col" width="10%">Foto</th>
                                <th scope="col" width="30%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                    <!-- endtabel -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade modal-slide-from-bottom" id="modaldokter" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="title" class="modal-title"></h5>
            </div>
            <div class="modal-body pb-0">
                <form id="formdokter" name="formdokter" method="post" action="{{route('simpan_dokter')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="">
                    <input type="hidden" name="id" value="">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Nama Rumah Sakit</label>
                                <select name="NAMA_RM" id="NAMA_RM" class="form-control">
                                    <option selected disabled></option>
                                    @foreach($optRumahSakit as $rs)
                                    <option value="{{$rs['id']}}">{{$rs['RS_NAMA']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Nama Spesialis</label>
                                <select name="NAMA_SPESIALIS" id="NAMA_SPESIALIS" class="form-control">
                                    <option selected disabled></option>
                                    @foreach($optSpesialis as $sp)
                                    <option value="{{$sp['id']}}">{{$sp['SP_NAMA']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Nama Dokter</label>
                                <input type="text" name="DOKTER_NAMA" value="" id="DOKTER_NAMA" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>HP</label>
                                <input type="text" name="DOKTER_HP" value="" id="DOKTER_HP" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Profil</label>
                                <input type="text" name="DOKTER_PROFIL" value="" id="DOKTER_PROFIL" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>NIP</label>
                                <input type="text" name="DOKTER_STR" value="" id="DOKTER_STR" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Foto</label>
                                <input type="file" name="gbr" id="gbr" class="form-control" value="" accept="image/*" onchange="readURL(this)">
                            </div>
                        </div>
                    </div>
                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview" class="form-group hidden" width="100" height="100">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
            <!-- <button class="btn btn-secondary" type="reset">Reset</button> -->
        </div>
    </div>
</div>
<!-- endModal -->
<script src="{{asset('assets/js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    var table = $('#tbdokter').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        "scrollY": "350px",
        "scrollCollapse": true,
        ajax: "{{ route('getDokter') }}",
        columns: [{

                data: "nomer",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }

            },
            {
                data: 'NAMA_RM',
                name: 'NAMA_RM'
            },
            {
                data: 'NAMA_SPESIALIS',
                name: 'NAMA_SPESIALIS'
            },
            {
                data: 'DOKTER_NAMA',
                name: 'DOKTER_NAMA'
            },
            {
                data: 'DOKTER_HP',
                name: 'DOKTER_HP'
            },
            {
                data: 'DOKTER_STR',
                name: 'DOKTER_STR'
            },
            {
                data: 'image',
                name: 'image'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });
    // tambah
    $(document).ready(function() {
        $(document).on('click', '#btnTambah', function(e) {
            $('#modaldokter').modal('show');
            $('#title').html('Tambah Dokter');
            $("#formdokter")[0].reset();
            $('input[name=action]').val('tambah');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        });
        // Edit
        $(document).on('click', '.btnEdit', function(e) {

            // console.log('OK');
            // alert('jjj');
            $('#title').html('Edit Dokter');
            $('input[name=action]').val('edit');
            var dokterid = $(this).data('id');
            var namarm = $(this).data('rm');
            var namaspesialis = $(this).data('spesialis');
            var dokternama = $(this).data('dokter');
            var dokterhp = $(this).data('hp');
            var dokterstr = $(this).data('str');
            var dokterprofil = $(this).data('profil');
            var doktergbr = $(this).data('gbr');
            $('input[name=DOKTER_ID]').val(dokterid);
            $('input[name=DOKTER_NAMA]').val(dokternama);
            $('input[name=DOKTER_PROFIL]').val(dokterprofil);
            $('input[name=DOKTER_HP]').val(dokterhp);
            $('input[name=DOKTER_STR]').val(dokterstr);
            $('input[name=gbr]').val(doktergbr);
            $('#NAMA_RM').val(namarm);
            $('#NAMA_SPESIALIS').val(namaspesialis);
            console.log(dokterprofil);
            console.log(namaspesialis);
            console.log(dokternama);
            $('#modal-preview').attr('src', 'http://localhost/alodocCoba/public/gb_dokter/' + doktergbr);
            $('#modaldokter').modal('show');
        });
        // delete
        $(document).on('click', '.delete', function(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yes, delete it!',
                        className: 'btn btn-success'

                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    console.log($(this).data('id'));
                    $.ajax({
                        url: '{{url()->current()}}/' + $(this).data('id'),
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            table.draw();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            //$('#modalOrg').modal('show');
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    });

    function readURL(input) {
        var file = $("input[type=file]").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function() {
                $("#modal-preview").attr('src', reader.result);
            };

            reader.readAsDataURL(file);
        }
    }
</script>
@endsection