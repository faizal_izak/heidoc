@extends('layouts.app')
@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['userid'];

@endphp
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Tabel Spesialis</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Tabel Spesialis</div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <!-- {{$token}} -->

                    <h4><i class="fas fa-pencil-ruler"></i> Tabel Spesialis</h4>
                </div>
                <button class="btn btn-primary" id="btnTambah">Tambah Data</button>
                <div class="card-body">
                    <!-- tabel -->
                    <table id="table_sp" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Spesialis</th>
                                <th scope="col" width="20%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                    <!-- endtabel -->
                </div>
            </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade modal-slide-from-bottom" id="modalspesialis" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="title" class="modal-title"></h5>
            </div>
            <div class="modal-body pb-0">
                <form id="formspesialis" name="formspesialis" method="post" action="{{route('simpan_spesialis')}}">
                    @csrf
                    <input type="hidden" name="action" value="">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Nama Spesialis</label>
                            <input type="text" name="SP_NAMA" value="" id="SP_NAMA" class="form-control">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endModal -->
<script src="{{asset('assets/js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    var table = $('#table_sp').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        "scrollY": "350px",
        "scrollCollapse": true,
        ajax: "{{ route('getSpesialis') }}",
        columns: [{

                data: "nomer",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }

            },
            {
                data: 'SP_NAMA',
                name: 'SP_NAMA'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });
    // tambah
    $(document).ready(function() {
        $(document).on('click', '#btnTambah', function(e) {
            $('#modalspesialis').modal('show');
            $('#title').html('Tambah Spesialis');
            $("#formspesialis")[0].reset();
            $('input[name=action]').val('tambah');
        });
        // Edit
        $(document).on('click', '.btnEdit', function(e) {
            // console.log('.btnEdit');
            // alert('jjj');
            $('#title').html('Edit Spesialis');
            $('input[name=action]').val('edit');
            var namaspesialis = $(this).data('namasp');
            var id = $(this).data('idsp');
            $('input[name=id]').val(id);
            $('input[name=SP_NAMA]').val(namaspesialis);

            $('#modalspesialis').modal('show');
        });
        // delete
        $(document).on('click', '.delete', function(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yes, delete it!',
                        className: 'btn btn-success'

                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    console.log($(this).data('idsp'));
                    $.ajax({
                        url: '{{url()->current()}}/' + $(this).data('idsp'),
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            table.draw();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            //$('#modalOrg').modal('show');
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    });
</script>
@endsection