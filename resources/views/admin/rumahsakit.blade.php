@extends('layouts.app')
@php
$data_session = Session::get('user_data');
$token = $data_session['token'];
$namaUser = $data_session['userid'];

@endphp
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Tabel Rumah Sakit</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Tabel Rumah Sakit</div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <!-- {{$token}} -->
                    <h4><i class="fas fa-hospital"></i> Tabel Rumah Sakit</h4>
                </div>
                <button class="btn btn-primary" id="btnTambah"><i class="fas fa-plus"></i> Tambah Data</button>
                <div class="card-body">
                    <table id="table_rs" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col" width="5%">No</th>
                                <th scope="col" width="15%">Nama Rumah Sakit</th>
                                <th scope="col" width="25%">Alamat</th>
                                <th scope="col" width="15%">Telepon</th>
                                <th scope="col" width="15%">Kota</th>
                                <th scope="col" width="15%">Foto</th>
                                <th scope="col" width="35%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade modal-slide-from-bottom" id="modalrs" aria-hidden="true">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="title" class="modal-title"></h5>
            </div>
            <div class="modal-body pb-0">
                <form id="formrs" name="formrs" method="post" action="{{route('simpan_rumahsakit')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="action" value="">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Nama Rumah Sakit</label>
                            <input type="text" name="RS_NAMA" id="RS_NAMA" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Alamat</label>
                            <input type="text" name="RS_ALAMAT" id="RS_ALAMAT" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Profil</label>
                            <input type="text" name="RS_PROFIL" id="RS_PROFIL" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Telepon</label>
                            <input type="text" name="RS_TELP" id="RS_TELP" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Kota</label>
                            <select name="RS_KOTA" id="RS_KOTA" class="form-control">
                                <option selected disabled></option>
                                @foreach($optKota as $value)
                                <option value="{{$value['id']}}">{{$value['nama_kota']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Foto</label>
                            <input type="file" name="gbr" id="gbr" class="form-control" value="" accept="image/*" onchange="readURL(this)">
                        </div>
                    </div>
                    <img id="modal-preview" src="https://via.placeholder.com/150" alt="Preview" class="form-group hidden" width="100" height="100">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- endModal -->
<script src="{{asset('assets/js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
    var table = $('#table_rs').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        "scrollY": "350px",
        "scrollCollapse": true,
        ajax: "{{ route('getRumahsakit') }}",
        columns: [{

                data: "nomer",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }

            },
            {
                data: 'RS_NAMA',
                name: 'RS_NAMA'
            },
            {
                data: 'RS_ALAMAT',
                name: 'RS_ALAMAT'
            },
            {
                data: 'RS_TELP',
                name: 'RS_TELP'
            },
            {
                data: 'namakota',
                name: 'namakota'
            },
            {
                data: 'image',
                name: 'image'

            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            },
        ]
    });
    // tambah

    $(document).ready(function() {
        $(document).on('click', '#btnTambah', function(e) {
            $('#modalrs').modal('show');
            $('#title').html('Tambah Data Rumah Sakit');
            $("#formrs")[0].reset();
            $('input[name=action]').val('tambah');
            $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        });
        // Edit
        $(document).on('click', '.btnEdit', function(e) {
            // console.log('.btnEdit');
            // alert('jjj');

            $('#title').html('Edit Data Rumah Sakit');
            $('input[name=action]').val('edit');

            var id = $(this).data('id');
            var rs_nama = $(this).data('rs_nama');
            var rs_alamat = $(this).data('rs_alamat');
            var rs_profil = $(this).data('rs_profil');
            var rs_telp = $(this).data('rs_telp');
            var rs_kota = $(this).data('kota');
            var gbr = $(this).data('gbr');
            $('input[name=id]').val(id);
            $('input[name=RS_NAMA]').val(rs_nama);
            $('input[name=RS_ALAMAT]').val(rs_alamat);
            $('input[name=RS_PROFIL]').val(rs_profil);
            $('input[name=RS_TELP]').val(rs_telp);
            $('#RS_KOTA').val(rs_kota);
            console.log(rs_kota);
            console.log(gbr);
            $('#modal-preview').attr('src', 'http://localhost/alodocCoba/public/gb_rs/' + gbr);
            // $('#gbr').val(gbr);

            $('#modalrs').modal('show');
        });
        // delete
        $(document).on('click', '.delete', function(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yes, delete it!',
                        className: 'btn btn-success'

                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    console.log($(this).data('id'));
                    $.ajax({
                        url: '{{url()->current()}}/' + $(this).data('id'),
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            table.draw();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                            //$('#modalOrg').modal('show');
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        type: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                } else {
                    swal.close();
                }
            });
        });
    });

    function readURL(input) {
        var file = $("input[type=file]").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function() {
                $("#modal-preview").attr('src', reader.result);
            };

            reader.readAsDataURL(file);
        }
    }
</script>
@endsection