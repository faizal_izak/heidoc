<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>ADMIN &mdash; Aplikasi Konsultasi Kesehatan</title>
    @include('layouts.headeradmin')
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            @include('layouts.navbar')
            <div class="main-sidebar">
                @include('layouts.dashboardadmin')
            </div>

            <!-- Main Content -->
            @yield('content')
            @include('layouts.footer')
        </div>
    </div>

    <!-- General JS Scripts -->
    <!-- dataTables -->

    <script src=" {{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/Bootstrap.min.js')}}"></script>
    <script src=" {{asset('assets/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src=" {{asset('assets/js/stisla.js')}}"></script>



    <!-- <script src=" {{asset('assets/js/dataTables.min.js')}}"></script>
    <script src=" {{asset('assets/js/dataTables.scroller.min.js')}}"></script> -->

    <!-- JS Libraies -->
    <!-- <script src="../node_modules/simpleweather/jquery.simpleWeather.min.js"></script>
    <script src="../node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="../node_modules/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="../node_modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../node_modules/summernote/dist/summernote-bs4.js"></script>
    <script src="../node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script> -->

    <!-- Template JS File -->
    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <script src="{{asset('assets/js/custom.js')}}"></script>


</body>

</html>