<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="{{url('/admin/landing')}}"><img src="{{asset('img/logo_heidoc.png')}}" width="130px" alt="Heidoc"></a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="index.html">St</a>
  </div>
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li class="nav-item active">
      <a href="#" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>

    </li>
    <li class="menu-header">Master Data</li>
    <li class="nav-">
      <a href="{{route('getDokter')}}" class="nav-link"><i class="fas fa-user-md"></i> <span>Dokter</span></a>
    </li>
    <li><a class="nav-link" href="{{route('getRumahsakit')}}"><i class="far fa-hospital"></i> <span>Rumah Sakit</span></a></li>
    <li class="nav-item dropdown">
      <a href="{{route('getJadwal')}}" class="nav-link"><i class="fas fa-calendar-check"></i> <span>Jadwal</span></a>
    </li>
    <li>
      <a class="nav-link" href="{{route('getSpesialis')}}"><i class="fas fa-pencil-ruler"></i> <span>Spesialis</span></a>
    </li>
    <li>
      <a class="nav-link" href="{{route('getKota')}}"><i class="fas fa-city"></i> <span>Kota</span></a>
    </li>
    <li>
      <a class="nav-link" href="{{route('getHistory')}}"><i class="fas fa-history"></i> <span>History</span></a>
    </li>
  </ul>

</aside>