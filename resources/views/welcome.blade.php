<!DOCTYPE html>
<html lang="en">

<head>
    <title>Welcome &mdash;Aplikasi Konsultasi Kesehatan</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="landing/css/bootstrap.min.css">
    <link rel="stylesheet" href="landing/css/jquery-ui.css">
    <link rel="stylesheet" href="landing/css/owl.carousel.min.css">
    <link rel="stylesheet" href="landing/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="landing/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="landing/css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="landing/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="landing/css/aos.css">

    <link rel="stylesheet" href="landing/css/style.css">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <div class="site-wrap">

        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>


        <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

            <div class="container-fluid">
                <div class="d-flex align-items-center">
                    <div class="site-logo mr-auto w-25"><a href="#">HEIDOC</a></div>

                    <div class="ml-auto w-25">
                        <nav class="site-navigation position-relative text-right" role="navigation">
                            <ul class="site-menu main-menu site-menu-dark js-clone-nav mr-auto d-none d-lg-block m-0 p-0">
                            </ul>
                        </nav>
                        <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a>
                    </div>
                </div>
            </div>

        </header>

        <div class="intro-section" id="home-section">

            <div class="slide-1" style="background-image: url('img/bg.jpg'); " data-stellar-background-ratio="0.1">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <div class="row align-items-center">
                                <div class="col-lg-12 mb-5 mt-0">
                                    <img src="img/logo_heidoc.png" alt="logo" style="display: block; margin: auto; width: 150px; height: 150px;">
                                    <h1 data-aos="fade-up" align="center" data-aos-delay="100">HALO ADMIN</h1>
                                    <p class="mb-4" align="center" data-aos="fade-up" data-aos-delay="200">Sistem Informasi Konsultasi Kesehatan</p>
                                    <p data-aos="fade-up" align="center" data-aos-delay="300">
                                        @if (Route::has('login'))
                                        @auth
                                        <a href="{{ route('logout') }}" class="btn btn-primary py-3 px-5 btn-pill" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        @else
                                        <a href="{{route('login')}}" class="btn btn-primary py-3 px-5 btn-pill">Start Login <i class="fas fa-signin"></i></a>
                                        @endif
                                        @endauth
                                    </p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <footer class="footer-section1 bg-white">
            <div class="container">

                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="border-top pt-5">
                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | Kelompok OJT <i class="icon-heart" aria-hidden="true"></i>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </footer>



    </div> <!-- .site-wrap -->

    <script src="landing/js/jquery-3.3.1.min.js"></script>
    <script src="landing/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="landing/js/jquery-ui.js"></script>
    <script src="landing/js/popper.min.js"></script>
    <script src="landing/js/bootstrap.min.js"></script>
    <script src="landing/js/owl.carousel.min.js"></script>
    <script src="landing/js/jquery.stellar.min.js"></script>
    <script src="landing/js/jquery.countdown.min.js"></script>
    <script src="landing/js/bootstrap-datepicker.min.js"></script>
    <script src="landing/js/jquery.easing.1.3.js"></script>
    <script src="landing/js/aos.js"></script>
    <script src="landing/js/jquery.fancybox.min.js"></script>
    <script src="landing/js/jquery.sticky.js"></script>


    <script src="landing/js/main.js"></script>

</body>

</html>