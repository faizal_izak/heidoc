@extends('layouts.dasboardadmin')

@section('sidebar')
<!-- Sidebar -->
<div class="sidebar">
	<!-- Sidebar Menu -->
	<nav class="mt-2">
		<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<li class="nav-item menu-open">
				<a href="{{url('admin')}}" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
					<p>Dashboard</p>
				</a>
			</li>
			<li class="nav-item menu-open">
				<a href="{{ url('/rumahsakit') }}" class="nav-link">
					<i class="nav-icon fas fa-bars"></i>&nbsp
					<p>Rumah Sakit</p>
				</a>
			</li>
			<li class="nav-item menu-open">
				<a href="{{ url('/spesialis') }}" class="nav-link">
					<i class="nav-icon fas fa-bars"></i>&nbsp
					<p>Spesialis</p>
				</a>
			</li>
			<li class="nav-item menu-open">
				<a href="{{ url('/dokter') }}" class="nav-link active">
					<i class="nav-icon fas fa-bars"></i>&nbsp
					<p>Dokter</p>
				</a>
			</li>
			<li class="nav-item menu-open">
				<a href="{{ url('/jadwal') }}" class="nav-link">
					<i class="nav-icon fas fa-bars"></i>&nbsp
					<p>Jadwal</p>
				</a>
			</li>
		</ul>
	</nav>
	<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
@endsection

@section('content')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Halaman Admin</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Beranda</a></li>
						<li class="breadcrumb-item active">Data Dokter</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>

	<section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Data Dokter</h3>
					</div>
					<div class="card-body">
						@if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
						@endif

						<div class="mb-2">
							<a href="{{ route('dokter.create') }}" class="btn btn-success" align='left'>+ Data Dokter</a>
							<input style="width:20%;float: right;" name="nm" type="text" class="form-control" id="nama" placeholder="masukkan nama dokter">
						</div>
						<table class="table table-bordered table-striped">
							<tr>
								<th width="15%">Foto</th>
								<th width="15%">Nama</th>
								<th width="10%">Spesialis</th>
								<th width="10%">Tempat Kerja</th>
								<th width="20%">Profil</th>
								<th width="10%">No. HP</th>
								<th width="10%">STR Dokter</th>
								<th width="10%">Action</th>
							</tr>
							@foreach($data as $row)
							<tr>
								<td><img src="/gb_dokter/{{ $row->DOKTER_GBR }}" class="img-thumbnail" width="75" /></td>
								<td>{{ $row->DOKTER_NAMA }}</td>
								<td>{{ $row->SP_NAMA }}</td>
								<td>{{ $row->RS_NAMA }}</td>
								<td>{{ $row->DOKTER_PROFIL }}</td>
								<td>{{ $row->DOKTER_HP }}</td>
								<td>{{ $row->DOKTER_STR }}</td>
								<td>

									<form action="{{ route('dokter.destroy', $row->DOKTER_ID) }}" method="post">
										{{ csrf_field() }}
										<a href="{{ route('dokter.show', $row->DOKTER_ID) }}" class="btn btn-success text-success bg-dark" style="width: 70px">Show</a>
										<a href="{{ route('dokter.edit', $row->DOKTER_ID) }}" class="btn btn-warning text-warning bg-dark" style="width: 70px">Edit</a>

										@method('DELETE')
										<button type="submit" class="btn btn-danger text-danger bg-dark" style="width: 70px">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</table>
						{!! $data->links() !!}

					</div>
				</div>
			</div>
		</div>
	</section>
</div>


@endsection