@extends('layouts.dasboardadmin')

@section('sidebar')
  <!-- Sidebar -->
  <div class="sidebar" >                  
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">                
        <li class="nav-item menu-open">
          <a href="{{url('admin')}}" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/rumahsakit') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Rumah Sakit</p>
          </a>               
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/spesialis') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Spesialis</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/dokter') }}" class="nav-link active">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Dokter</p>
          </a>                
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/jadwal') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Jadwal</p>
          </a>                
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
@endsection

@section('content')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Halaman Admin</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{url('admin')}}">Beranda</a></li>
					<li class="breadcrumb-item"><a href="{{route('dokter.index')}}">Dokter</a></li>
					<li class="breadcrumb-item active">Tambah Data Dokter</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>

	<section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Data Dokter</h3>
					</div>
					<div class="card-body">
							
						<form method="post" action="{{ route('dokter.update', $data->DOKTER_ID) }}" enctype="multipart/form-data">
							@csrf
							@method('PATCH')
							<div class="row p-3">
								<div class="col-md-6">
									<div class="form-group  mb-3">
										<label for="nama">Nama</label>
										<input  value="{{$data->DOKTER_NAMA}}" type="text" class="form-control @error('nm')
										is-invalid @enderror" id="nama" placeholder="masukkan nama dokter"
										name="nm">
										@error('nm')
										<div class="invalid-feedback">{{$message}}</div>
										@enderror
									</div>
							
									<div class="form-group  mb-3">
										<label for="spesialis">Spesialis</label>
											<select class="custom-select @error('sp')
											is-invalid @enderror" id="spesialis" name="sp">s
											<option value="" selected>pilih spesialis</option>
												@foreach($spesialis as $sp)
													<option value="{{$sp -> id}}"  {{ $data->SP_ID == $sp -> id ? 'selected' : ''}}>{{$sp -> SP_NAMA}}</option>
												@endforeach
											</select>
											@error('sp')
											<div class="invalid-feedback">{{$message}}</div>
											@enderror
									</div>
							
									<div class="form-group  mb-3">
										<label for="rumah_sakit">Tempat Kerja</label>
											<select class="custom-select @error('rs')
											is-invalid @enderror" id="rumah_sakit" name="rs">
											<option value="" selected>pilih RS</option>
												@foreach($rs as $drs)
													<option value="{{$drs -> RS_ID}}" {{ $data->RS_ID == $drs -> RS_ID ? 'selected' : ''}}>{{$drs -> RS_NAMA}}</option>
												@endforeach
											</select>
											@error('rs')
											<div class="invalid-feedback">{{$message}}</div>
											@enderror
									</div>
								</div>
							
								<div class="col-md-6">
									<div class="form-group ">
										<label for="noHP">No. HP</label>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">+62</span>
											</div>
											<input value="{{substr($data->DOKTER_HP,3)}}" type="text" class="form-control @error('hp') 
											is-invalid @enderror" id="noHP" placeholder="masukkan No. HP dokter"
											name="hp">
											@error('hp')
											<div class="invalid-feedback">{{$message}}</div>
											@enderror
										</div>  
									</div>
							
									<div class="form-group  mb-3">
										<label for="str">STR Dokter</label>
										<input name="str" type="text" class="form-control @error('str') 
										is-invalid @enderror" id="str" placeholder="STR(Surat Tanda Registrasi)"
										value="{{$data->DOKTER_STR}}" >
										@error('str')
										<div class="invalid-feedback">{{$message}}</div>
										@enderror
									</div>
							
									<div class="form-group  mb-3">
										<div class="row">
											<div class="col-md-3">
												<img src="{{ URL::to('/') }}/gb_dokter/{{ $data->DOKTER_GBR }}" class="img-thumbnail" width="90" style="position: relative"/>
											</div>
											<div class="col-md-9">
											<label for="customFile">Pilih Foto Dokter</label>
												<div class="custom-file">
												<input name="gbDr" type="file" class="custom-file-input" id="customFile">
												<label class="custom-file-label" for="customFile">Choose file</label>
										</div>
										</div>
										</div>
									</div>
								  
									<script>
										$(".custom-file-input").on("change", function() {
										var fileName = $(this).val().split("\\").pop();
										$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
										});
									</script> 

									</div>
									<div class="col-md-12">
										<div class="form-group ">
											<label for="profil">Profil Dokter</label>
											<textarea name="pf" class="form-control @error('pf') is-invalid @enderror" id="profil" rows="2">
											{{$data->DOKTER_PROFIL}}</textarea>
											@error('pf')
											<div class="invalid-feedback">{{$message}}</div>
											@enderror
										</div>
										<div class="form-group  mb-3">
											<input type="submit" name="add" class="btn btn-warning" value="Update" />
											<a href="{{ route('dokter.index') }}" class="btn btn-secondary">Kembali</a>
										</div>
									</div>
							</div>
						</form>						

				</div>
			</div>
		</div>			
	</section>	
</div>


@endsection



