@extends('dokter.parentDK')

@section('main')
<div class="container shadow">
@if($errors->any())
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<div class="container">
	<center><h2 style="float: left;padding-top:10px;position:absolute">Update Data Dokter</h2></center>
	<a style="float: right;" href="{{ route('dokter.index') }}" class="btn bg-danger text-light">X</a>
</div>
<form method="post" action="{{ route('dokter.update', $data->DOKTER_ID) }}" enctype="multipart/form-data">
	@csrf
	@method('PATCH')

<br><br>
<input type="hidden" name="hidden_image" value="{{$data->DOKTER_GBR}}" />

<div class="row p-3">
<div class="col-md-6">
	<div class="form-group  mb-3">
		<label for="nama">Nama</label>
	<input name="nm" value="{{$data->DOKTER_NAMA}}" type="text" class="form-control" id="nama" placeholder="masukkan nama dokter" >
	</div>

	<div class="form-group  mb-3">
		<label for="spesialis">Spesialis</label>
			<select class="custom-select" id="spesialis" name="sp">
			  <option selected>pilih spesialis</option>
				  @foreach($spesialis as $sp)
				  <option value="{{$sp -> id}}" {{ $data->SP_ID == $sp -> id ? 'selected' : ''}}>{{$sp -> SP_NAMA}}</option>
				  @endforeach
			</select>
	</div>

	<div class="form-group  mb-3">
		<label for="rumah_sakit">Tempat Kerja</label>
			<select class="custom-select" id="rumah_sakit" name="rs">
			  <option selected>pilih RS</option>
				  @foreach($rs as $drs)
				  <option value="{{$drs -> RS_ID}}" {{ $data->RS_ID == $drs -> RS_ID ? 'selected' : ''}}>{{$drs -> RS_NAMA}}</option>				  
				  @endforeach
			</select>
	</div>
</div>

<div class="col-md-6">
	<div class="form-group ">
		<label for="noHP">No. HP</label>
		<div class="input-group mb-3">
			<div class="input-group-prepend">
			<span class="input-group-text" id="basic-addon1">+62</span>
			</div>
			<input name="hp" value="{{substr($data->DOKTER_HP,3)}}" type="text" class="form-control" placeholder="Nomor HP" aria-label="Username" aria-describedby="basic-addon1" id="noHP">
		</div>  
	</div>

	<div class="form-group  mb-3">
		<label for="str">STR Dokter</label>
		<input name="str" value="{{$data->DOKTER_STR}}" type="text" class="form-control" id="str" placeholder="STR(Surat Tanda Registrasi)">
	</div>

	<div class="form-group  mb-3">
		<div class="row">
		<div class="col-md-3">
			<img src="{{ URL::to('/') }}/gb_dokter/{{ $data->DOKTER_GBR }}" class="img-thumbnail" width="90" style="position: relative"/>
		</div>
		<div class="col-md-9">
			<label for="customFile">Pilih Foto Dokter</label>
			<div class="custom-file">
				<input name="gbDr" type="file" class="custom-file-input" id="customFile">
				<label class="custom-file-label" for="customFile">Choose file</label>
			</div>
		</div>
		</div>
	</div>
  
	<script>
		$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
	</script> 
</div>
<div class="col-md-12">
	<div class="form-group ">
		<label for="profil">Profil Dokter</label>
	<textarea name="pf" class="form-control" id="profil" rows="2">{{$data->DOKTER_PROFIL}}</textarea>
	</div>
	<div class="form-group  mb-3">
		<input type="submit"  name="edit" class="btn btn-warning" value="Update" />
	</div>
</div>
</div>

</form>
</div>
@endsection



