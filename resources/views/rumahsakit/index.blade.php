@extends('layouts.dasboardadmin')

@section('sidebar')
  <!-- Sidebar -->
  <div class="sidebar" >                  
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">                
        <li class="nav-item menu-open">
          <a href="{{url('admin')}}" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/rumahsakit') }}" class="nav-link active">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Rumah Sakit</p>
          </a>               
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/spesialis') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Spesialis</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/rumahsakit') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Dokter</p>
          </a>                
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/jadwal') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Jadwal</p>
          </a>                
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Halaman Admin</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin')}}">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Rumah Sakit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Rumah Sakit</h3>
                        </div>
                        <div class="card-body">
                            <a href="{{ url('/rumahsakit/create') }}" class="btn btn-primary my-3">Tambah Data Rumah Sakit</a>
                            <br>

                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    <th scope="col">No.</th>                                        
                                    <th scope="col">Nama Rumah Sakit</th>                    
                                    <th scope="col">Alamat</th>                    
                                    <th scope="col">Telepon</th>                                                                        
                                    <th scope="col">Profil Rumah Sakit</th>                    
                                    <th scope="col">Foto</th>                    
                                    <th scope="col">Maps</th>                    
                                    <th scope="col">Aksi</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rumahsakit as $data)
                                    <tr>                        
                                        <td>{{ $loop->iteration }}</td>                                                                                                            
                                        <td>{{ $data->RS_NAMA }}</td>                                                                                        
                                        <td>{{ $data->RS_ALAMAT }}</td>                                                                                        
                                        <td>{{ $data->RS_TELP }}</td>                                                                                        
                                        <td>{{ $data->RS_PROFIL }}</td>                                                                                        
                                        <td>{{ $data->RS_GBR }}</td>                                                                                        
                                        <td>{{ $data->RS_MAPS }}</td>                                                                                        
                                        <td>
                                            <a href="#" class="btn btn-success">Detail</a>                            
                                            <a href="{{ url('/rumahsakit') }}/{{$data->id}}/edit" class="btn btn-warning">Ubah</a>                            
                                            <form action="{{ url('/rumahsakit') }}/{{$data->id}}" method="post" class="d-inline">
                                              @method('delete')
                                              @csrf
                                              <button type="submit" class="btn btn-danger" onclick="return confirm('Yakin untuk menghapus data?')">Hapus</button>
                                            </form>                       
                                        </td>                                                         
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection