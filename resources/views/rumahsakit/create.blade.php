@extends('layouts.dasboardadminadd')

@section('sidebar')
  <!-- Sidebar -->
  <div class="sidebar" >                  
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">                
        <li class="nav-item menu-open">
          <a href="{{url('admin')}}" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Dashboard</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/rumahsakit') }}" class="nav-link active">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Rumah Sakit</p>
          </a>               
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/spesialis') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Spesialis</p>
          </a>
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/dokter') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Dokter</p>
          </a>                
        </li>
        <li class="nav-item menu-open">
          <a href="{{ url('/jadwal') }}" class="nav-link">
            <i class="nav-icon fas fa-bars"></i>&nbsp
            <p>Jadwal</p>
          </a>                
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Halaman Admin</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/rumahsakit') }}">Data Rumah Sakit</a></li>
                        <li class="breadcrumb-item active">Tambah Data Rumah Sakit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Rumah Sakit</h3>
                        </div>
                        <div class="card-body">                                                      
                            <form method="post" action="{{ url('/rumahsakit') }}">     
                                @csrf    
                                
                                <div class="form-group row">
                                    <label for="RS_NAMA" class="col-sm-2 col-form-label">Nama Rumah Sakit</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_NAMA')
                                        is-invalid @enderror" id="RS_NAMA" placeholder=""
                                        name="RS_NAMA" value="{{ old('RS_NAMA') }}">
                                        @error('RS_NAMA')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="RS_ALAMAT" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_ALAMAT')
                                        is-invalid @enderror" id="RS_ALAMAT" placeholder=""
                                        name="RS_ALAMAT" value="{{ old('RS_ALAMAT') }}">
                                        @error('RS_ALAMAT')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="RS_TELP" class="col-sm-2 col-form-label">Telepon</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_TELP')
                                        is-invalid @enderror" id="RS_TELP" placeholder=""
                                        name="RS_TELP" value="{{ old('RS_TELP') }}">
                                        @error('RS_TELP')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="RS_PROFIL" class="col-sm-2 col-form-label">Tentang Rumah Sakit</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_PROFIL')
                                        is-invalid @enderror" id="RS_PROFIL" placeholder=""
                                        name="RS_PROFIL" value="{{ old('RS_PROFIL') }}">
                                        @error('RS_PROFIL')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="RS_GBR" class="col-sm-2 col-form-label">Foto</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_GBR')
                                        is-invalid @enderror" id="RS_GBR" placeholder=""
                                        name="RS_GBR" value="{{ old('RS_GBR') }}">
                                        @error('RS_GBR')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="form-group row">
                                    <label for="RS_MAPS" class="col-sm-2 col-form-label">Maps</label>
                                    <div class="col-sm-4">
                                        <input type="TEXT" class="form-control @error('RS_MAPS')
                                        is-invalid @enderror" id="RS_MAPS" placeholder=""
                                        name="RS_MAPS" value="{{ old('RS_MAPS') }}">
                                        @error('RS_MAPS')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @enderror
                                    </div>                                                                                               
                                </div>
                                <div class="text-right">
                                <button type="submit" class="btn btn-primary">Tambah</button>    
                                <a href="{{ url('/rumahsakit') }}" class="btn btn-secondary">Kembali</a>                                                   
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection