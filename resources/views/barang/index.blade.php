@extends('layouts.dasboardadmin')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
	
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Halaman Admin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin')}}">Beranda</a></li>
              <li class="breadcrumb-item active">Data Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    
    <section class="content">
      <div class="row">
        <div class="col-12">
    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Barang
              
              </h3>
            </div>
          <div class="card-body">
            <div class="row">
  </form>
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<a href="javascript:void(0)" class="btn btn-success mb-2" id="new-barang" data-toggle="modal">Tambah Barang</a>
<a href="barangpdf" class="btn btn-success pull-right"
          style="margin-top:-8px" ><i class="fas fa-print"></i> Cetak PDF </a><br>
</div>
</div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
<p id="msg">{{ $message }}</p>
</div>
@endif
<table class="table table-bordered" id="table_barang">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Jumlah</th>
                <th>Harga</th>
                <th width="280px">Action</th>
            </tr>
        </thead>
        <tbody>
          @foreach ($barang as $data)
          <tr id="barang_id_{{ $data->id }}">
          <td>{{ $data->id }}</td>
          <td>{{ $data->nama_barang }}</td>
          <td>{{ $data->qty }}</td>
          <td>{{ number_format($data->harga,0) }}</td>
          <td>
          <form action="{{ route('barang.destroy',$data->id) }}" method="POST">
          <a class="btn btn-info" id="show-barang" data-toggle="modal" data-id="{{ $data->id }}" >Show</a>
          <a href="javascript:void(0)" class="btn btn-success" id="edit-barang" data-toggle="modal" data-id="{{ $data->id}}">Edit </a>
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <a id="delete-barang" data-id="{{ $data->id }}" class="btn btn-danger delete-user">Delete</a></td>
          </form>
          </td>
          </tr>
          @endforeach
          </tbody>
</table>
<!-- Add and Edit barang modal -->
<div class="modal fade" id="crud-modal" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="barangcrudmodal"></h4>
</div>
<div class="modal-body">
<form name="custForm" action="{{ route('barang.store') }}" method="POST">
<input type="hidden" name="cust_id" id="cust_id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Nama Barang:</strong>
<input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Name" onchange="validate()" >
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Merk:</strong>
<input type="text" name="merk" id="merk" class="form-control" placeholder="Merk" onchange="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Jumlah:</strong>
<input type="text" name="qty" id="qty" class="form-control" placeholder="Jumlah" onchange="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Satuan:</strong>
<input type="text" name="satuan" id="satuan" class="form-control" placeholder="Satuan" onchange="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Harga:</strong>
<input type="text" name="harga" id="harga" class="form-control" placeholder="Harga" onchange="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" disabled>Submit</button>
<a href="{{ route('barang.index') }}" class="btn btn-danger">Cancel</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- Show barang modal -->
<div class="modal fade" id="crud-modal-show" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="showbarangmodal"></h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-xs-2 col-sm-2 col-md-2"></div>
<div class="col-xs-10 col-sm-10 col-md-10 ">
@if(isset($data->nama_barang))

<table>
<tr><td><strong>Nama Barang:</strong></td><td>{{$data->nama_barang}}</td></tr>
<tr><td><strong>Merk:</strong></td><td>{{$data->merk}}</td></tr>
<tr><td><strong>Jumlah:</strong></td><td>{{$data->qty}}</td></tr>
<tr><td><strong>Satuan:</strong></td><td>{{$data->satuan}}</td></tr>
<tr><td><strong>Harga:</strong></td><td>{{number_format($data->harga,0)}}</td></tr>
<tr><td colspan="2" style="text-align: right "><a href="{{ route('barang.index') }}" class="btn btn-danger">OK</a> </td></tr>
</table>
@endif
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 </div>
</div>
</section>

<script>
error=false

function validate()
{
  if(document.custForm.nama_barang.value !='' && document.custForm.merk.value !='' && document.custForm.qty.value !='' && document.custForm.satuan.value !='' && document.custForm.harga.value !='')
      document.custForm.btnsave.disabled=false
  else
    document.custForm.btnsave.disabled=true
}
</script>

<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#table_barang').DataTable();
} );
</script>

<script>
$(document).ready(function () {

/* When click Add jasa button */
$('#new-barang').click(function () {
$('#btn-save').val("create-barang");
$('#barang').trigger("reset");
$('#barangcrudmodal').html("Tambah Barang");
$('#crud-modal').modal('show');
});

/* Edit Jasa */
$('body').on('click', '#edit-barang', function () {
var data_id= $(this).data('id');
$.get('barang/'+data_id+'/edit', function (data) {
$('#barangcrudmodal').html("Edit Barang ");
$('#btn-update').val("Update");
$('#btn-save').prop('disabled',false);
$('#crud-modal').modal('show');
$('#cust_id').val(data.id);
$('#nama_barang').val(data.nama_barang);
$('#merk').val(data.merk);
$('#qty').val(data.qty);
$('#satuan').val(data.satuan);
$('#harga').val(data.harga);
})
});
/* Show barang */
$('body').on('click', '#show-barang', function () {
$('#showbarangmodal').html("Detail Barang");
$('#crud-modal-show').modal('show');
});

/* Delete Jasa */
$('body').on('click', '#delete-barang', function () {
var data_id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");

$.ajax({
type: "DELETE",
url: "http://localhost/djenbengkel/public/barang/"+data_id,
data: {
"id": data_id,
"_token": token,
},
success: function (data) {
$('#msg').html('barang Berhasil Dihapus');
$("#barang_id_" +data_id).remove();
},
error: function (data) {
console.log('Error:', data);
}
});
});
});
</script>
@endsection