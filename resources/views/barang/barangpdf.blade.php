<html>
<head>
	<title>BARANG PDF </title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 align = "center">DAFTAR BARANG</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
        <thead>
          <tr>
            <th> ID Barang </th>
            <th> Nama Barang </th>
            <th> Merk </th>
            <th> Jumlah</th>
            <th> Satuan</th>
            <th> Harga</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($barang as $data)

          <tr>
            <td> {{ $data -> id}}</td>
            <td> {{ $data -> nama_barang}}</td>
            <td> {{ $data -> merk}}</td>
            <td> {{ $data -> qty}}</td>
            <td> {{ $data -> satuan}}</td>
            <td> {{ $data -> harga}}</td>
          </tr>
          @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</body>
	</html>