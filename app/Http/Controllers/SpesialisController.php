<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\LastSession;
use App\Spesialis;
use GuzzleHttp\Client;
use DataTables;
use AppHelper;

class SpesialisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }

    public function index(Request $request)
    {
        $optSpesialis = AppHelper::optSpesialis();
        if ($request->ajax()) {
            $this->cek();
            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getSpesialis/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataSpesialis = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
            // return $DataDokter;
            return DataTables::of($DataSpesialis)
                ->addColumn('nomer', function ($data) {
                    $no = 1;
                    return $no;
                })
                ->rawColumns(['nomer'])
                ->addColumn('action', function ($data) {
                    $button = ' <a href="#" class="btn btn-warning btnEdit" 
                    data-idsp="' . $data['id'] . '" 
                    data-namasp="' . $data['SP_NAMA'] . '" 
                    data-original-title="Edit">
                    <i class="fas fa-edit " aria-hidden="true"></i></a>';
                    $button = $button . '<a href="javascript:void(0)"  class="btn btn-danger delete" data-idsp="' . $data['id'] . '" data-toggle="tooltip" data-original-title="Remove">
                    <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.spesialis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_spesialis(Request $request)
    {
        $this->cek();



        $data_array = array(
            'SP_NAMA' => $request->SP_NAMA,
        );
        $data_update = array(
            'id' => $request->id
        );
        // dd($data_array);
        if ($request->action == 'tambah') {
            $url = 'http://127.0.0.1/ojt_RS_api/public/postInsSpesialis';
            $params['headers'] = $this->myHeader;
            $params['form_params'] = $data_array;
            $response = $this->Client->post($url, $params);
            // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
            $DataSpesialis = \GuzzleHttp\json_decode($response->getBody(), true);

            // dd($Dataseminar);
        } else if ($request->action == 'edit') {

            $url = "http://127.0.0.1/ojt_RS_api/public/postUpdSpesialis";
            $params['headers'] = $this->myHeader;
            $params['form_params'] = array_merge($data_array + $data_update);
            $response = $this->Client->post($url, $params);
            $DataSpesialis = \GuzzleHttp\json_decode($response->getBody(), true);
            // dd($DataPenataran);
        }
        return redirect($_SERVER['HTTP_REFERER']);
        //    dd($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spesialis $spesialis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_spesialis($id)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelSpesialis";
        $params['headers'] = $this->myHeader;
        $id = array(
            "id" => $id
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $DataSpesialis = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
