<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use Session;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use AppHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth_admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function adminHome(Request $request)
    {
        return $data_session = Session::get('user_app');
        $header = AppHelper::headerToken();
        // dd($peg_id);

        // $this->cek();
        $Client = AppHelper::loadfile();
        $url ="http://127.0.0.1/ojt_RS_api/public/login";
        // dd($url);
        #Send request by http method=>GET
        $params['headers'] = $header;
        $params['form_params'] = [
            'username' => 'admin',
            'password' => 'admin123'
        ];   
        $response = $Client->post($url, $params);
        #Receive request by http method=>GET
        return $datas=\GuzzleHttp\json_decode($response->getBody(), true);
        //isset tidak bekerja jika data berupa JSON, maka jika data berupa JSON perlu di Decode terlebih dahulu
   
        
        return $request->session()->all();
        return 'ini admin';
    }
}
