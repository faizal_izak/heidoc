<?php
  
namespace App\Http\Controllers\Auth;
   
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
  
    use AuthenticatesUsers;
  
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
   
    private function validate_input($request, $id = null)
	{

		return Validator::make(
			$request->all(),
			[
				'username' => 'required',
				'password' => 'required',
			]
		);
    }

    public function login(Request $request)
    {   
        // dd($request);
        if ($request->isMethod('get')) {
			return redirect('login');
		}

		$validation = $this->validate_input($request);

		if ($validation->fails()) {
			return 'ok';
		}
		
		$username = $request->username;
		$password = $request->password;
        // dd($request->all());
        
        // $response = Http::post('localhost/ojt_RS_api/public/login', [
        //     'username' => $username,
        //     'password' => $password,
        // ]);
        // return $response ;

		$client = new Client(['verify' => public_path('ssl/cacert.pem')]);
		$myBody = array(
			"username" => $username,
			"password" => $password,
        );

        $headers = ['Content-Type' => 'application/json'];
        
        $params['form_params'] = $myBody;  


		$url = "127.0.0.1/ojt_RS_api/public/login";
        
		try {
            $request = $client->post($url,$params);
            $login = \GuzzleHttp\json_decode($request->getBody(), true);
			// Here the code for successful request
		
		} catch (RequestException $e) {
			// if ($e->getResponse()->getStatusCode() == '400') {
			// 		echo "Got response 400";
			// }
			// $request = $client->post($url,['form_params'=>$myBody]);
			// $login = \GuzzleHttp\json_decode($request->getBody(), true);
			return redirect('login')->withErrors('Username atau Password Salah')->withInput();
			// You can check for whatever error status code you need 
		
		} catch (\Exception $e) {		
			// There was another exception.
		}

        if (isset($login['result'])) {
            
            $data = $login['result'];

            if ($data != null) {
                    $data_user = [];
                    $data_user['userid'] = $data['id'];
                    $data_user['username'] = $data['username'];
                    $data_user['name'] = $data['name'];
                    $data_user['level'] = $data['level'];
                    $data_user['email'] = $data['email'];
                    $data_user['token'] = $data['token'];
                    session::put('user_data', $data_user);

                    // Setting landing page per USer
                    if ($data_user['level']=='admin') {
                        return redirect('admin/landing');
                    } elseif ($data_user['level']=='dokter') {
                        return redirect('dokter/landing');
                    }elseif ($data_user['level']=='user') {
                        return redirect('user/landing');
                    }
			
			} else {
                // dd('eror');
				return redirect('login')->withErrors('Username Belum Diatur')->withInput();
			}
		} else {
            // dd('eror');
			return redirect('login')->withErrors('Username atau Password Salah')->withInput();
		}
          
    }

    public function logout()
	{
		session_start();

		$user = session::get('user_data');
		$id = trim($user['userid']);
		
		$client = new Client(['verify' => public_path('ssl/cacert.pem')]);
		$myBody = array(
			"id" => $id,
		);
		$url = "localhost/ojt_RS_api/public/logout";
		try {
			$request = $client->post($url,['form_params'=>$myBody]);
			$login = \GuzzleHttp\json_decode($request->getBody(), true);
			// Here the code for successful request
		
		} catch (RequestException $e) {
			// if ($e->getResponse()->getStatusCode() == '400') {
			// 		echo "Got response 400";
			// }
			$login = array('pesan'=>'error');
			// You can check for whatever error status code you need 
		} catch (\Exception $e) {
			// There was another exception.
		}

		Session::flush();

		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach ($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name  = trim($parts[0]);
				setcookie($name, '', time() - 1000);
				setcookie($name, '', time() - 1000, '/');
			}
		}

		$_SESSION = array();

		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(
				session_name(),
				'',
				time() - 42000,
				$params["path"],
				$params["domain"],
				$params["secure"],
				$params["httponly"]
			);
		}

		session_destroy();

		return redirect('/login');
	}    
}