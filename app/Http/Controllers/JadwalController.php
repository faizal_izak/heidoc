<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\LastSession;
use GuzzleHttp\Client;
use AppHelper;
use DataTables;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $Client;
    private $token;
    private $myHeader;
    public function cek()
    {
        $data_session = Session::get('user_app');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }
    public function index(Request $request)
    {
        $optDokter = AppHelper::optDokter();
        if ($request->ajax()) {
            $this->cek();

            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getJadwal/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataJadwal = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
            // return $DataJadwal;
            return DataTables::of($DataJadwal)
                ->addColumn('nomer', function ($data) {
                    $no = 1;
                    return $no++;
                })
                ->rawColumns(['nomer'])
                ->addColumn('action', function ($data) {
                    $button = ' <a href="javascript:void(0)" class="btn btn-warning btnEdit" data-id="' . $data['JADWAL_ID'] . '" data-dokter="' . $data['DOKTER_ID'] . '"  data-tgl="' . $data['JADWAL_TGL'] . '" data-mulai="' . $data['JADWAL_JAM_M'] . '" data-selesai="' . $data['JADWAL_JAM_S'] . '" data-original-title="Edit">
            <i class="fas fa-edit " aria-hidden="true"></i></a>';
                    $button = $button . '<a href="#" class="btn btn-danger delete" data-id="' . $data['JADWAL_ID'] . '" data-toggle="tooltip" data-original-title="Remove">
            <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.jadwal', compact('optDokter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_jadwal(Request $request)
    {
        $this->cek();

        $data_array = array(
            'DOKTER_ID' => $request->DOKTER_ID,
            'JADWAL_TGL' => $request->JADWAL_TGL,
            'JADWAL_JAM_M' => $request->JADWAL_JAM_M,
            'JADWAL_JAM_S' => $request->JADWAL_JAM_S


        );
        $data_update = array(
            'JADWAL_ID' => $request->JADWAL_ID
        );
        // dd($data_array);
        if ($request->action == 'tambah') {
            $url = 'http://127.0.0.1/ojt_RS_api/public/postInsJadwal';
            $params['headers'] = $this->myHeader;
            $params['form_params'] = $data_array;
            $response = $this->Client->post($url, $params);
            // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
            $DataKota = \GuzzleHttp\json_decode($response->getBody(), true);

            // dd($Dataseminar);
        } else if ($request->action == 'edit') {

            $url = "http://127.0.0.1/ojt_RS_api/public/postUpdJadwal";
            $params['headers'] = $this->myHeader;
            $params['form_params'] = array_merge($data_array + $data_update);
            $response = $this->Client->post($url, $params);
            $DataKota = \GuzzleHttp\json_decode($response->getBody(), true);
            // dd($DataPenataran);
        }
        return redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_jadwal($JADWAL_ID)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelJadwal";
        $params['headers'] = $this->myHeader;
        $id = array(
            "JADWAL_ID" => $JADWAL_ID
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
