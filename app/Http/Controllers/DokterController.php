<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Session;
use App\LastSession;
use App\Tb_RS;
use GuzzleHttp\Client;
use DataTables;
use AppHelper;

class DokterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }

    public function index(Request $request)
    {
        $optRumahSakit = AppHelper::optRumahSakit();
        $optSpesialis = AppHelper::optSpesialis();
        if ($request->ajax()) {
            $this->cek();
            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getDokter/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataDokter = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
            // return $DataDokter;
            return DataTables::of($DataDokter)
                ->addColumn('image', function ($data) {
                    $img = '<img src="' . url('gb_dokter') . '/' .   $data['DOKTER_GBR'] . '" id="Preview" alt="Preview" name="Preview" class="img-thumbnail" width="75" />';
                    return $img;
                })
                ->addColumn('action', function ($data) {
                    $button = ' <a href="javascript:void(0)" class="btn btn-warning btnEdit" 
                    data-id="' . $data['DOKTER_ID'] . '" 
                    data-rm="' . $data['RS_ID'] . '" 
                    data-spesialis="' . $data['SP_ID'] . '"
                    data-dokter="' . $data['DOKTER_NAMA'] . '"
                    data-hp="' . $data['DOKTER_HP'] . '"
                    data-profil="' . $data['DOKTER_PROFIL'] . '"
                    data-str="' . $data['DOKTER_STR'] . '"
                    data-gbr="' . $data['DOKTER_GBR'] . '"  data-original-title="Edit">
                    <i class="fas fa-edit " aria-hidden="true"></i></a>';
                    $button = $button . '<a href="javascript:void(0)"  class="btn btn-danger delete" data-id="' . $data['DOKTER_ID'] . '" data-toggle="tooltip" data-original-title="Remove">
                    <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['image', 'action'])
                ->make(true);
        }
        return view('admin.dokter', compact('optRumahSakit', 'optSpesialis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_dokter(Request $request)
    {
        $this->cek();

        $file = $request->file('gbr');

        $nama_file = time() . "_" . $file->getClientOriginalName();


        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'gb_dokter';
        $file->move($tujuan_upload, $nama_file);


        $data_array = array(
            'RS_ID' => $request->RS_ID,
            'SP_ID' => $request->SP_ID,
            'DOKTER_NAMA' => $request->DOKTER_NAMA,
            'DOKTER_RM' => $request->DOKTER_RM,
            'NAMA_SPESIALIS' => $request->NAMA_SPESIALIS,
            'DOKTER_PROFIL' => $request->DOKTER_PROFIL,
            'DOKTER_HP' => $request->DOKTER_HP,
            'DOKTER_STR' => $request->DOKTER_STR,
            'DOKTER_GBR' => $nama_file,


        );
        $data_update = array(
            'DOKTER_ID' => $request->DOKTER_ID
        );
        // dd($data_array);
        if ($request->action == 'tambah') {
            $url = 'http://127.0.0.1/ojt_RS_api/public/postInsDokter';
            $params['headers'] = $this->myHeader;
            $params['form_params'] = $data_array;
            $response = $this->Client->post($url, $params);
            // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
            $DataDokter = \GuzzleHttp\json_decode($response->getBody(), true);

            // dd($Dataseminar);
        } else if ($request->action == 'edit') {

            $url = "http://127.0.0.1/ojt_RS_api/public/postUpdDokter";
            $params['headers'] = $this->myHeader;
            $params['form_params'] = array_merge($data_array + $data_update);
            $response = $this->Client->post($url, $params);
            $DataDokter = \GuzzleHttp\json_decode($response->getBody(), true);
            // dd($DataPenataran);
        }
        return redirect($_SERVER['HTTP_REFERER']);
        //    dd($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_dokter($id)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelDokter";
        $params['headers'] = $this->myHeader;
        $id = array(
            "DOKTER_ID" => $id
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $DataDokter = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
