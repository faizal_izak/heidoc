<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\LastSession;
use App\RumahSakit;
use GuzzleHttp\Client;
use DataTables;
use File;
use AppHelper;

class RumahSakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }

    public function index(Request $request)
    {
        $optKota = AppHelper::optKota();
        if ($request->ajax()) {
            $this->cek();
            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getRumahsakit/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET

            return DataTables::of($DataRumahsakit)

                ->addColumn('image', function ($data) {
                    $img = '<img src="' . url('gb_rs') . '/' .   $data['RS_GBR'] . '" id="Preview" alt="Preview" name="Preview" class="img-thumbnail" width="75" />';
                    return $img;
                })
                ->addColumn('action', function ($data) {

                    $button = ' <a href="#" class="btn btn-warning btnEdit" data-id="' . $data['id'] . '"  data-gbr="' . $data['RS_GBR'] . '" data-kota="' . $data['RS_KOTA'] . '" data-rs_nama="' . $data['RS_NAMA'] . '" data-rs_alamat="' . $data['RS_ALAMAT'] . '" data-rs_telp="' . $data['RS_TELP'] . '" data-rs_profil="' . $data['RS_PROFIL'] . '" data-original-title="Edit">
                    <i class="fas fa-edit " aria-hidden="true"></i></a>';
                    $button = $button . '<a href="javascript:void(0)" class="btn btn-danger delete" data-id="' . $data['id'] . '" data-toggle="tooltip" data-original-title="Remove">
                    <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['image', 'action'])
                ->make(true);
        }
        return view('admin.rumahsakit', compact('optKota'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_rumahsakit(Request $request)
    {
        $this->cek();

        $file = $request->file('gbr');

        $nama_file = time() . "_" . $file->getClientOriginalName();


        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'gb_rs';
        $file->move($tujuan_upload, $nama_file);

        $data_array = array(
            'RS_NAMA' => $request->RS_NAMA,
            'RS_ALAMAT' => $request->RS_ALAMAT,
            'RS_TELP' => $request->RS_TELP,
            'RS_PROFIL' => $request->RS_PROFIL,
            'RS_GBR' => $nama_file,
            // 'RS_GBR' => $request->RS_GBR,
            'RS_KOTA' => $request->RS_KOTA,


        );
        $data_array1 = array(
            'RS_NAMA' => $request->RS_NAMA,
            'RS_ALAMAT' => $request->RS_ALAMAT,
            'RS_TELP' => $request->RS_TELP,
            'RS_PROFIL' => $request->RS_PROFIL,
            // 'RS_GBR' => $request->RS_GBR,
            'RS_KOTA' => $request->RS_KOTA,


        );
        $data_update = array(
            'id' => $request->id
        );
        // dd($data_array);
        if ($request->action == 'tambah') {
            $url = 'http://127.0.0.1/ojt_RS_api/public/postInsRumahsakit';
            $params['headers'] = $this->myHeader;
            $params['form_params'] = $data_array;
            $response = $this->Client->post($url, $params);
            // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
            $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true);

            // dd($Dataseminar);
        } else if ($request->action == 'edit') {
            if ($request->$nama_file == '') {

                $url = "http://127.0.0.1/ojt_RS_api/public/postUpdRumahsakit";
                $params['headers'] = $this->myHeader;
                $params['form_params'] = array_merge($data_array + $data_update);
                $response = $this->Client->post($url, $params);
                $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true);
            } else
                $url = "http://127.0.0.1/ojt_RS_api/public/postUpdRumahsakit";
            $params['headers'] = $this->myHeader;
            $params['form_params'] = array_merge($data_array1 + $data_update);
            $response = $this->Client->post($url, $params);
            $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true);
            // dd($DataPenataran);
        }
        return redirect($_SERVER['HTTP_REFERER']);
        //    dd($request);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_rumahsakit($id)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelRumahsakit";
        $params['headers'] = $this->myHeader;
        $id = array(
            "id" => $id
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $DataRumahsakit = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
