<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Barang;
use PDF;
use Redirect,Response;
use Yajra\Datatables\Datatables;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cetakpdf()
    {
    	$barang = Barang::all();
        
        $no=0;
        $pdf = PDF::loadview('barang.barangpdf',compact('barang','no'));
        $pdf->setPaper('a4','potrait');
    	return $pdf->stream();
    }
    public function json(){

        return Datatables::of(Barang::query())->make(true);
    }
    public function index()
    {
            // mengambil data dari table barang
        $barang = DB::table('barang')->paginate();
 
            // mengirim data barang ke view index
        return view('barang.index',['barang' => $barang]);
 
    }
public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;
 
            // mengambil data dari table barang sesuai pencarian data
        $barang = DB::table('barang')
        ->where('nama_barang','like',"%".$cari."%")
        ->paginate();
 
            // mengirim data barang ke view index
        return view('barang.index',['barang' => $barang]);
 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $r=$request->validate([
        'nama_barang' => 'required',
        'merk' => 'required',
        'qty' => 'required',
        'satuan' => 'required',
        'harga' => 'required'
        ]);

        $id = $request->cust_id;
        Barang::UpdateOrCreate(['id' => $id],['nama_barang' => $request->nama_barang, 'merk' => $request->merk, 'qty' => $request->qty, 'satuan' => $request->satuan, 'harga'=>$request->harga]);
        if(empty($request->cust_id))
            $msg = 'Barang Berhasil Ditambahkan.';
        else
            $msg = 'Barang Berhasil di Ubah';
        return redirect()->route('barang.index')->with('success',$msg);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        return view('barang.show',compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $barang = Barang::where($where)->first();
        return Response::json($barang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $barang = Barang::where('id',$id)->delete();
        return Response::json($barang);
    }
}
