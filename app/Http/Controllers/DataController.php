<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;

class DataController extends Controller
{
    public function getDataRS(Request $request)
    {
        //return csrf_token();
        $nm_rs = $request['nama_rs'];
        $data = \DB::table('tb_rs')
            ->select(DB::raw(" RS_ID,RS_NAMA,RS_ALAMAT,RS_TELP,RS_PROFIL,CONCAT('http://192.168.43.156/coba_Laravel7/public/gb_rs/',RS_GBR) AS 'url'"))
            ->where('RS_NAMA', 'like', "%" . $nm_rs . "%")
            ->get();
        return $data;
    }

    public function getDataDokter(Request $request)
    {
        $nm_rs = $request['nama_rs'];
        $dataDokRS = DB::select("SELECT DOKTER_ID,DOKTER_NAMA,SP_NAMA as nama_sp,DOKTER_PROFIL,DOKTER_HP,DOKTER_STR,
        CONCAT('localhost/OJT/coba_Laravel7/public/gb_dokter/',DOKTER_GBR) AS 'DOKTER_GBR',RS_NAMA
        FROM
        tb_dokter d, tb_spesialis s, tb_rs r
        WHERE d.`SP_ID` = s.`SP_ID`
        and r.rs_id = d.rs_id
        AND RS_NAMA = ?", [$nm_rs]);
        return $dataDokRS;
    }

    public function getNamaSpesialis()
    {
        $dataSp = DB::select("SELECT SP_NAMA from tb_spesialis order by SP_NAMA desc;");
        return $dataSp;
    }

    public function getDataDokterSp(Request $request)
    {
        $nm_sp = $request['nama_sp'];
        $dataDokSp = DB::select("SELECT DOKTER_ID,DOKTER_NAMA,SP_NAMA as nama_sp,DOKTER_PROFIL,DOKTER_HP,DOKTER_STR,
        CONCAT('192.168.43.126/coba_Laravel7/public/gb_dokter/',DOKTER_GBR) AS 'DOKTER_GBR',RS_NAMA
        FROM
        tb_dokter d, tb_spesialis s, tb_rs r
        WHERE d.`SP_ID` = s.`SP_ID`
        and r.rs_id = d.rs_id
        AND SP_NAMA = ?", [$nm_sp]);
        return $dataDokSp;
    }

    public function getDataDokterLengkap(Request $request)
    {
        $nm_dok = $request['nama_dokter'];
        $dataDoklkp = DB::select("SELECT DOKTER_ID,
        CONCAT('http://192.168.43.156/coba_Laravel7/public/gb_dokter/',DOKTER_GBR) AS 'DOKTER_GBR',
        CONCAT('http://192.168.43.156/coba_Laravel7/public/gb_rs/',RS_GBR) AS 'url',
        DOKTER_NAMA,RS_NAMA,SP_NAMA,DOKTER_PROFIL,DOKTER_HP,DOKTER_STR
        FROM tb_dokter d, tb_spesialis s, tb_rs r
        WHERE d.`SP_ID` = s.`id`
        AND d.`RS_ID` = r.`RS_ID`
        and DOKTER_NAMA = ?", [$nm_dok]);
        return $dataDoklkp;
    }

    public function getJadwalDokter(Request $request)
    {
        $nm_dok = $request['nama_dokter'];
        $jadwalDok = DB::select("CALL jadwaL(?)", [$nm_dok]);
        return $jadwalDok;
    }
}
