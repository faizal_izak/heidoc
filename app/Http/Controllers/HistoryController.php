<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Session;
use App\LastSession;
use GuzzleHttp\Client;
use DataTables;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $Client;
    private $token;
    private $myHeader;
    public function cek()
    {
        $data_session = Session::get('user_app');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $this->cek();
            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getHistory/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataHistory = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
            // return $DataHistory;
            return DataTables::of($DataHistory)
                ->addColumn('nomer', function ($data) {
                    $no = 1;
                    return $no;
                })
                ->rawColumns(['nomer'])
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)"  class="btn btn-danger delete" data-id="' . $data['id'] . '" data-toggle="tooltip" data-original-title="Remove">
                    <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.history');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_history($id)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelHistory";
        $params['headers'] = $this->myHeader;
        $id = array(
            "id" => $id
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $Dataseminar = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
