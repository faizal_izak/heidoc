<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\LastSession;
use GuzzleHttp\Client;
use DataTables;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $Client;
    private $token;
    private $myHeader;

    public function cek()
    {
        $data_session = Session::get('user_data');

        $this->token = $data_session['token'];
        $this->Client =  new Client(['verify' => public_path('ssl/cacert.pem')]);
        $this->myHeader = array(
            "api-token" => $this->token,
        );
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $this->cek();
            #get data pemberhentian dari API
            $url = "http://127.0.0.1/ojt_RS_api/public/getKota/";
            $response = $this->Client->get($url, ['headers' => $this->myHeader]); #Send request by http method=>GET

            $DataKota = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
            // return $DataKota;
            return DataTables::of($DataKota)
                ->addColumn('nomer', function ($data) {
                    $no = 1;
                    return $no;
                })
                ->rawColumns(['nomer'])
                ->addColumn('action', function ($data) {
                    $button = ' <a href="#" class="btn btn-warning btnEdit" data-id="' . $data['id'] . '" data-nama_kota="' . $data['nama_kota'] . '"  data-original-title="Edit">
                    <i class="fas fa-edit " aria-hidden="true"></i></a>';
                    $button = $button . '<a href="javascript:void(0)"  class="btn btn-danger delete" data-id="' . $data['id'] . '" data-toggle="tooltip" data-original-title="Remove">
                    <i class="fas fa-trash" aria-hidden="true"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.kota');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_kota(Request $request)
    {
        $this->cek();



        $data_array = array(
            'nama_kota' => $request->nama_kota,


        );
        $data_update = array(
            'id' => $request->id
        );
        // dd($data_array);
        if ($request->action == 'tambah') {
            $url = 'http://127.0.0.1/ojt_RS_api/public/postInsKota';
            $params['headers'] = $this->myHeader;
            $params['form_params'] = $data_array;
            $response = $this->Client->post($url, $params);
            // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
            $DataKota = \GuzzleHttp\json_decode($response->getBody(), true);

            // dd($Dataseminar);
        } else if ($request->action == 'edit') {

            $url = "http://127.0.0.1/ojt_RS_api/public/postUpdKota";
            $params['headers'] = $this->myHeader;
            $params['form_params'] = array_merge($data_array + $data_update);
            $response = $this->Client->post($url, $params);
            $DataKota = \GuzzleHttp\json_decode($response->getBody(), true);
            // dd($DataPenataran);
        }
        return redirect($_SERVER['HTTP_REFERER']);
        //    dd($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus_kota($id)
    {
        $this->cek();

        $url = "http://127.0.0.1/ojt_RS_api/public/postDelKota";
        $params['headers'] = $this->myHeader;
        $id = array(
            "id" => $id
        );
        $params['form_params'] = $id;
        $response = $this->Client->post($url, $params);
        // $response = $Client->post($url,['headers'=>$hearder],['form_params'=>$data_array]);
        $Dataseminar = \GuzzleHttp\json_decode($response->getBody(), true);
        // dd($DataDiklatFungsional);
        return redirect($_SERVER['HTTP_REFERER']);
    }
}
