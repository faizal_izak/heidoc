<?php

namespace App\Http\Controllers;

use App\Tb_kota;
use Illuminate\Http\Request;

class TbKotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tb_kota  $tb_kota
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_kota $tb_kota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tb_kota  $tb_kota
     * @return \Illuminate\Http\Response
     */
    public function edit(Tb_kota $tb_kota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tb_kota  $tb_kota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_kota $tb_kota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tb_kota  $tb_kota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_kota $tb_kota)
    {
        //
    }
}
