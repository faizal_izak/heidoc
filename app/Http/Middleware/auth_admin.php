<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class auth_admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$user = session::get('user_data');
        $level = trim($user['level']);
                
        if($level == 'admin'){
            return $next($request);
        } 
        return redirect('/')->with('error',"You don't have admin access.");        
        
    }
}
