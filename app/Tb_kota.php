<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_kota extends Model
{
    protected $table = 'tb_kotas';
    protected $primaryKey = 'id';
}
