<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_Spesialis extends Model
{
    protected $table = 'tb_spesialis';
    protected $primaryKey = 'id';
    protected $fillable = [
        'SP_NAMA'
       ];

}
