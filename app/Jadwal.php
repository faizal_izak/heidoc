<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'tb_jadwal';    
    protected $fillable=['DOKTER_ID', 'JADWAL_HARI', 'JADWAL_WAKTU', 'JADWAL_JAM_M', 'JADWAL_JAM_S'];

    public function dokter(){
    	return $this->belongsTo('App\Dokter');
    }
}
