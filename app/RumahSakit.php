<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RumahSakit extends Model
{
    protected $table = 'tb_rs';
    protected $fillable = ['RS_NAMA', 'RS_ALAMAT', 'RS_TELP', 'RS_PROFIL', 'RS_GBR', 'RS_MAPS'];
}
