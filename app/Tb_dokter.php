<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_dokter extends Model
{
    protected $table = 'tb_dokter';
    protected $primaryKey = 'DOKTER_ID';
    protected $fillable = [
        'RS_ID', 'SP_ID', 'DOKTER_NAMA','DOKTER_PROFIL','DOKTER_HP','DOKTER_STR','DOKTER_GBR'
       ];

}
