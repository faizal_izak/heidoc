<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'tb_dokter';    
    protected $fillable=['RS_ID', 'SP_ID', 'DOKTER_NAMA', 'DOKTER_PROFIL', 'DOKTER_HP', 'DOKTER_STR', 'DOKTER_GBR'];

    public function rs(){
    	return $this->belongsTo('App\RumahSakit');
    }
    public function spesialis(){
    	return $this->belongsTo('App\Spesialis');
    }
    public function jadwal(){
    	return $this->belongsTo('App\Jadwal');
    }
}
