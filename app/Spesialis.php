<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spesialis extends Model
{
    protected $table = 'tb_spesialis';    
    protected $fillable=['SP_NAMA'];

    public function dokter(){
    	return $this->belongsTo('App\Dokter');
    }
}