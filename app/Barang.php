<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
  protected $table = 'barang';
  protected $primaryKey='id';
  protected $fillable=['nama_barang', 'merk', 'qty', 'satuan', 'harga'];

 public function transaksi(){
    	return $this->hasMany('App\Transaksi', 'id');
    }
    public function pembelian(){
      return $this->hanMany('App\Pembelian', 'id');
    }
    	
}
