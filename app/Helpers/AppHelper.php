<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use DB;

class AppHelper
{

    public static function loadfile()
    {
        $file = new Client(['verify' => public_path('ssl/cacert.pem')]);
        return $file;
    }

    public static function headerToken()
    {
        $data_session = Session::get('user_app');
        $token = $data_session['remember_token'];
        $myHeader = array(
            "api-token" => $token,
        );
        return $myHeader;
    }
    public static function optDokter()
    {
        $client = AppHelper::loadfile();
        $token = AppHelper::headerToken();

        #get data Penghargaan dari API
        $url = "http://127.0.0.1/ojt_RS_api/public/getDokter/";
        $response = $client->get($url, ['headers' => $token]); #Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
        return $data;
    }
    public static function optKota()
    {
        $client = AppHelper::loadfile();
        $token = AppHelper::headerToken();

        #get data Penghargaan dari API
        $url = "http://127.0.0.1/ojt_RS_api/public/getKota/";

        $response = $client->get($url, ['headers' => $token]); #Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
        return $data;
    }

    public static function optSpesialis()
    {
        $client = AppHelper::loadfile();
        $token = AppHelper::headerToken();

        $url = "http://127.0.0.1/ojt_RS_api/public/getSpesialis/";

        $response = $client->get($url, ['headers' => $token]); #Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
        return $data;
    }
    public static function optRumahSakit()
    {
        $client = AppHelper::loadfile();
        $token = AppHelper::headerToken();

        #get data Penghargaan dari API
        $url = "http://127.0.0.1/ojt_RS_api/public/getRumahsakit/";
        $response = $client->get($url, ['headers' => $token]); #Send request by http method=>GET

        $data = \GuzzleHttp\json_decode($response->getBody(), true); #Receive request by http method=>GET
        return $data;
    }
}
