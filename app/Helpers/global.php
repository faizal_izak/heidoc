<?php

use App\Dokter;
use App\Jadwal;
use App\RumahSakit;
use App\Spesialis;
use App\Tb_kota;

function totalDokter()
{
	return Dokter::count();
}
function totalRumahsakit()
{
	return RumahSakit::count();
}
function totalJadwal()
{
	return Jadwal::count();
}
function totalSpesialis()
{
	return Spesialis::count();
}
function totalKota()
{
	return Tb_kota::count();
}
