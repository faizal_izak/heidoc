-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2020 at 11:42 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rs`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_dokter`
--

CREATE TABLE `tb_dokter` (
  `id` int(11) NOT NULL,
  `RS_ID` int(11) DEFAULT NULL,
  `SP_ID` int(11) DEFAULT NULL,
  `DOKTER_NAMA` varchar(50) DEFAULT NULL,
  `DOKTER_PROFIL` longtext,
  `DOKTER_HP` varchar(15) DEFAULT NULL,
  `DOKTER_STR` varchar(25) DEFAULT NULL,
  `DOKTER_GBR` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dokter`
--

INSERT INTO `tb_dokter` (`id`, `RS_ID`, `SP_ID`, `DOKTER_NAMA`, `DOKTER_PROFIL`, `DOKTER_HP`, `DOKTER_STR`, `DOKTER_GBR`, `created_at`, `updated_at`) VALUES
(3, 3, 3, 'dr. Sujan Ali Fing, Sp.M', 'dr. Sujan Ali Fing, SpM, MSOphth merupakan Dokter Spesialis Mata yang saat ini aktif berpraktik di Rumah Sakit Khusus Mata Prima Vision, Medan. Sebelum bergabung di RS Khusus Mata Prima Vision, Beliau merupakan Chief of Ophthalmologist di RS Columbia Asia Medan. Beliau terhimpun dalam Perhimpunan Dokter Spesialis Mata Indonesia (PERDAMI) dan Ikatan Dokter Indonesia (IDI).\r\n\r\nSebagai salah satu lulusan terbaik dari Fakultas Kedokteran Umum, Universitas Sumatera Utara, beliau memperoleh beasiswa penuh dari Lee Foundation, Singapore, untuk melanjutkan Spesialis Mata (Master of Surgery, Ophthalmology) di National University Hospital, Malaysia (HUKM) dan juga telah menyelesaikan fellowship di bidang Vitreoretina. Adapun layanan yang dr. Sujan berikan meliputi Pemeriksaan dan Konsultasi Mata.', '+6289098876452', '99.1.1.603.4.20.084833', 'dr_sujan.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(4, 5, 3, 'dr. Ditha Paramasitha, Sp.M', 'dr. Ditha Paramasitha, Sp.M merupakan Dokter Mata yang menamatkan pendidikan Spesialisnya di Universitas Brawijaya, Malang. Beliau saat ini aktif berpraktik di Rumah Sakit Columbia Semarang sebagai Dokter Mata.\r\n\r\ndr. Ditha Paramasitha terhimpun dalam Perhimpunan Dokter Ahli Mata Indonesia dan dapat membantu memberikan layanan meliputi : Konsultasi perihal kesehatan mata.', '+6281234348822', '35.2.1.603.2.18.121698', 'dr_Ditha.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(5, 4, 3, 'dr. Puti Ayu Tiara, Sp.M', 'dr. Puti Ayu Tiara, Sp.M merupakan Dokter spesialis Mata yang berpraktek di Netra Klinik Spesialis Mata Tangerang. Beliau menyelesaikan studi spesialis Mata di Universitas Padjadjaran, Bandung. Beliau juga merupakan anggota Persatuan Dokter Mata Indonesia (PERDAMI) dan anggota Ikatan Dokter Indonesia (IDI). Layanan kesehatan yang beliau berikan meliputi : Konsultasi perihal kesehatan mata.', '+6280942112331', '32.2.1.603.2.19.109715', 'dr_puti.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(9, 10, 4, 'dr. R. Amanda Sumantri, Sp.KK', 'dr. R. Amanda Sumantri, Sp.KK merupakan Dokter Spesialis Kulit dan Kelamin dengan pengalaman lebih dari 5 tahun. dr. Amanda berpraktek di Rumah Sakit Premier Jatinegara sebagai Dokter Spesialis Kulit dan Kelamin. Beliau menamatkan pendidikan Kedokteran Spesialis Kulit dan Kelamin di Universitas Indonesia pada tahun 2013.\r\n\r\nBeliau adalah anggota dari Ikatan Dokter Indonesia (IDI), Perhimpunan Dokter Spesialis Kulit dan Kelamin (PERDOSKI). Adapun layanan yang diberikan yaitu terkait Konsultasi perihal kesehatan kulit dan kelamin.', '+6289664488412', '31.2.1.602.3.18.090181', 'dr_R_amanda.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(10, 6, 4, 'dr. Irvan Satyawan, Sp.KK', 'dr. Irvan Satyawan, Sp.KK adalah seorang Dokter Spesialis Kulit dan Kelamin yang berpraktek di Siloam Hospitals Kebon Jeruk. Beliau menamatkan pendidikan Kedokteran Spesialis Kulit dan Kelamin di Royal Netherlands Medical Association Rotterdam.\r\n\r\ndr. Irvan Satyawan, Sp.KK merupakan anggota dari Ikatan Dokter Indonesia (IDI), Perhimpunan Dokter Spesialis Kulit dan Kelamin Indonesia (PERDOSKI). Beliau dapat membantu memberikan bantuan pelayanan medis terkait pengobatan Kulit dan Kelamin.', '+6284471516848', '99.1.1.602.3.16.037997', 'dr_irvan_satyawan.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(11, 12, 8, 'dr. Ria Yolanda Vitri, Sp.PD', 'dr. Ria Yolanda Vitri, Sp.PD merupakan seorang Dokter Spesialis Penyakit Dalam di RSIA Al Islam Bandung, Jawa Barat. Beliau menamatkan pendidikan Spesialisnya di Fakultas Kedokteran Universitas Padjadjaran Bandung. \r\n\r\ndr. Ria Yolanda Vitri, Sp.PD merupakan anggota dari Ikatan Dokter Indonesia (IDI) dan Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia (PAPDI). Beliau dapat memberikan bantuan pelayanan medis terkait pengobatan penyakit dalam.', '+6286548946541', '32.2.1.401.3.17.101550', 'dr_Ria.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(12, 7, 8, 'dr. Deshinta Putri Mulya, Sp.PD-KAI (K)', 'dr. Deshinta Putri Mulya, Sp.PD-KAI (K) adalah seorang Dokter Spesialis Penyakit Dalam yang mengambil konsultan Alergi dan lmunologi. Saat ini, beliau berpraktik di Rumah Sakit JIH, Yogyakarta. Adapun layanan medis yang dapat beliau berikan meliputi : Konsultasi perihal penyakit dalam. Selain itu, beliau juga tercatat sebagai anggota dari Ikatan Dokter Indonesia (IDI) dan Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia (PAPDI).\r\n\r\ndr. Deshinta Putri Mulya, Sp.PD-KAI (K) menamatkan pendidikan Master of Science in Internal Medicine, FK UGM (2006 - 2008); Spesialis Penyakit Dalam, FK UGM (2006 - 2010); dan Pendidikan Konsultan Alergi lmunologi, FK UI (2012 - 2015). Adapun berbagai pelatihan yang pernah beliau ikuti, di antaranya World Allergy Training, SatGas lmunisasi, Jakarta (2013); Vaccination Training, SatGas lmunisasi Jakarta (2013); Basic And Advance Immunology Training AAAI (American Association of Allergy and Immunology) Philadelphia; Pennsylvania, USA (2013); ABBAS Course of Basic Immunology University of Kualalumpur, Malaysia (2013); Musketeers Immunology Course GMU (2012).', '+6284489849489', '34.2.1.401.3.15.083085', 'dr_Deshinta.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(13, 8, 7, 'dr. Brain Gantoro, Sp.GK', 'dr. Brain Gantoro, Sp.GK adalah seorang Dokter Gizi. Saat ini, dr. Brain Gantoro, Sp.GK berpraktek di Rumah Sakit Awal Bros Batam sebagai Dokter Gizi Klinik. Adapun layanan yang beliau berikan meliputi : Konsultasi Keseimbangan Gizi.\r\n\r\ndr. Brain Gantoro, Sp.GK menamatkan pendidikan Spesialis Gizi Klinik di Universitas Indonesia. Beliau tergabung dalam anggota Ikatan Dokter Indonesia (IDI).', '+6283321648465', '31.1.1.701.4.18.048301', 'dr_Brain.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(14, 6, 5, 'dr. Adelena Anwar, Sp.T.H.T.K.L', 'dr. Lia Natalia, Sp.THT adalah seorang Dokter Spesialis Telinga Hidung dan Tenggorok - Bedah Kepala Leher yang telah menamatkan pendidikan Spesialisnya di Universitas Gadjah Mada, Yogyakarta. Beliau terdaftar dalam Perhimpunan Telinga Hidung Tenggorok Bedah Kepala Leher Indonesia.\r\n\r\ndr. Lia Natalia berpraktik di Rumah Sakit Mayapada Tangerang dan Rumah Sakit Mitra Keluarga Gading Serpong sebagai Dokter Spesialis THT-KL. Beliau memiliki dapat memberikan bantuan pelayanan medis terkait dengan pengobatan dan perawatan terhadap Telinga Hidung Tenggorokan.', '+6285778111896', '34.2.1.606.3.16.093736', 'dr_adelena.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(15, 11, 6, 'dr. Renindra Ananda, SpBS', 'Dr. dr. Renindra Ananda Aman, Sp.BS(K) adalah Dokter spesialis bedah saraf yang berpraktik di RS Pelni Jakarta. Beliaumenamatkan pendidikan dokter umum dari Fakultas Kedokteran Universitas Indonesia tahun 1989, dan melanjutkan program spesialisasi bedah saraf yang lulus pada tahun 1999. Beliau kemudian meraih gelar Doktor dari Universitas Indonesia pada tahun 2008.\r\n\r\ndr. Renindra mendapatkan kesempatan memperdalam ilmunya dengan mendapatkan fellowship di Eropa dan Asia, yaitu Fellowship in Neuro-Oncology with Neuronavigation Techniques and In Parkinson’s disease with Functional Stereotactic Surgery di Amsterdam tahun 2001 dan Fellowship in Microsurgery, Neurovascular surgery, Endovascular procedures and Pituitary Tumors di Jepang tahun 2002.\r\n\r\nBeliau dapat memberikan bantuan pelayanan medis terkait Bedah Saraf, seperti : konsultasi sebelum tindakan pada saraf.', '+6284945484949', '31.1.1.105.3.16.049822', 'dr_renindra.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(16, 9, 9, 'dr. Manish Taneja', 'Dr Manish Taneja adalah radiolog. Beliau lulus dari Maulana Azad Medical College of Delhi University tahun 1995. Setelah pelatihan di India, ia pindah ke Inggris, di mana ia memperoleh Nomor Pelatihan Nasional dalam radiologi di All Wales Program Pelatihan Tinggi setelah pelatihan medis pertama. Beliau di latih sebagai Registrar Spesialis selama lima lima tahun dan dianugerahi Radiologi Sertifikat Kelulusan Pelatihan Specialis UK pada tahun 2005, sehingga memungkinkan dia untuk praktek sebagai konsultan di Inggris.\r\n\r\nTahun 2005, Dr Taneja pindah ke Kanada mengikuti satu tahun beasiswa untuk pembuluh darah dan radiologi intervensi di University Health Network meliputi Toronto General Hospital, Mount Sinai Hospital, Princess Margaret Hospital dan Toronto Western Hospital. Beliau dilatih dalam semua aspek prosedur radiologi invasif minimal termasuk arteri perifer dan intervensi endovascular vena, hepatobilier intervensi, intervensi onkologi, intervensi ginekologi, ginjal dan intervensi dialysis terkait. Dr Taneja juga mendapat beasiswa yang lain untuk neuroradiologi intervensi setelah menyelesaikan ujian lisensi AS. Beliau mendapat beasiswa satu tahun di University Of Washington School of Medicine, yang terdiri dari Harborview Medical Center, University of Washington Medical Centre dan Seattle Children’s Hospital. Beliau dilatih dalam semua aspek prosedur invasive minimal di otak, kepala dan leher, serta tulang belakang, dengan penekanan khusus pada pengobatan endovascular dari aneurisma otak, stroke dan arteri-vena malformasi.\r\n\r\nSebelum bergabung dengan Raffles Hospital, Dr Taneja bekerja di Singapore General Hospital. Beliau memiliki berbagai pengalaman dalam semua radiologis dipandu prosedur invasive minimal, baik pembuluh darah dan intervensi non-vascular. Beliau juga memiliki minat khusus dalam neuroradiologi diagnostik dan pencitraan pembuluh darah.', '+6284941519916', '-', 'dr_manish_taneja.jpg', '2020-06-06 05:42:44', '0000-00-00 00:00:00'),
(17, 3, 9, 'dr Isyana', 'Kosong', '085123456789', '0000 0000 0000 0000', 'Loading', '2020-06-06 02:45:40', '2020-06-06 02:45:40'),
(18, 2, 3, 'dr Isyana', 'Kosong', '085123456789', '0000', 'Loading', '2020-06-06 12:03:05', '2020-06-06 05:03:05'),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-06 08:16:49', '2020-06-06 08:16:49'),
(20, 12, 8, 'dr Isyana', 'Kosong', '085123456789', '0000', 'Loading', '2020-06-07 08:34:19', '2020-06-07 01:34:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jadwal`
--

CREATE TABLE `tb_jadwal` (
  `id` int(11) NOT NULL,
  `DOKTER_ID` int(11) DEFAULT NULL,
  `JADWAL_HARI` varchar(10) DEFAULT NULL,
  `JADWAL_WAKTU` varchar(10) DEFAULT NULL,
  `JADWAL_JAM_M` time DEFAULT NULL,
  `JADWAL_JAM_S` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jadwal`
--

INSERT INTO `tb_jadwal` (`id`, `DOKTER_ID`, `JADWAL_HARI`, `JADWAL_WAKTU`, `JADWAL_JAM_M`, `JADWAL_JAM_S`, `created_at`, `updated_at`) VALUES
(5, 3, 'senin', 'pagi', '08:00:00', '14:00:00', NULL, NULL),
(6, 3, 'selasa', 'sore', '13:00:00', '18:00:00', NULL, NULL),
(7, 3, 'selasa', 'pagi', '07:00:00', '10:00:00', NULL, NULL),
(8, 4, 'senin', 'pagi', '07:00:00', '10:00:00', NULL, NULL),
(9, 4, 'kamis', 'pagi', '07:00:00', '10:00:00', NULL, NULL),
(10, 5, 'selasa', 'siang', '11:00:00', '14:00:00', NULL, NULL),
(11, 5, 'rabu', 'siang', '11:00:00', '14:00:00', NULL, NULL),
(16, 9, 'jumat', 'malam', '18:00:00', '21:00:00', NULL, NULL),
(17, 10, 'kamis', 'siang', '11:00:00', '14:00:00', NULL, NULL),
(18, 10, 'jumat', 'pagi', '07:00:00', '10:00:00', NULL, NULL),
(19, 11, 'senin', 'pagi', '07:00:00', '09:00:00', NULL, NULL),
(20, 11, 'senin', 'malam', '18:00:00', '21:00:00', NULL, NULL),
(21, 11, 'selasa', 'siang', '14:00:00', '17:00:00', NULL, NULL),
(22, 12, 'kamis', 'sore', '15:00:00', '18:00:00', NULL, NULL),
(23, 12, 'jumat', 'pagi', '07:00:00', '09:00:00', NULL, NULL),
(24, 12, 'jumat', 'sore', '15:00:00', '18:00:00', NULL, NULL),
(25, 13, 'rabu', 'siang', '14:00:00', '17:00:00', NULL, NULL),
(26, 13, 'rabu', 'malam', '19:00:00', '21:00:00', NULL, NULL),
(27, 14, 'sabtu', 'pagi', '08:00:00', '12:00:00', NULL, NULL),
(28, 14, 'senin', 'siang', '11:00:00', '15:00:00', NULL, NULL),
(29, 14, 'senin', 'malam', '18:00:00', '21:00:00', NULL, NULL),
(30, 15, 'rabu', 'malam', '18:00:00', '21:00:00', NULL, NULL),
(31, 16, 'senin', 'pagi', '09:00:00', '11:00:00', NULL, NULL),
(32, 16, 'senin', 'malam', '17:00:00', '20:00:00', NULL, NULL),
(33, 16, 'rabu', 'sore', '15:00:00', '19:00:00', NULL, NULL),
(34, 16, 'sabtu', 'siang', '13:00:00', '17:00:00', NULL, '2020-06-05 02:02:32'),
(36, 4, 'jumat', 'siang', '10:00:00', '16:00:00', NULL, '2020-06-05 22:29:44'),
(37, 3, 'rabu', 'siang', '11:00:00', '18:00:00', '2020-06-05 02:18:15', '2020-06-06 03:37:31'),
(38, 3, 'Senin', 'Pagi', '11:00:00', '15:00:00', '2020-06-06 03:37:04', '2020-06-06 04:08:15'),
(39, 3, 'Senin', 'Pagi', '07:00:00', '11:00:00', '2020-06-06 03:51:11', '2020-06-06 04:08:02'),
(41, 3, 'Senin', 'Pagi', '11:00:00', '17:00:00', '2020-06-07 01:33:48', '2020-06-07 01:33:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rs`
--

CREATE TABLE `tb_rs` (
  `id` int(11) NOT NULL,
  `RS_NAMA` varchar(30) DEFAULT NULL,
  `RS_ALAMAT` varchar(30) DEFAULT NULL,
  `RS_TELP` varchar(15) DEFAULT NULL,
  `RS_PROFIL` longtext,
  `RS_GBR` varchar(50) DEFAULT NULL,
  `RS_MAPS` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_rs`
--

INSERT INTO `tb_rs` (`id`, `RS_NAMA`, `RS_ALAMAT`, `RS_TELP`, `RS_PROFIL`, `RS_GBR`, `RS_MAPS`, `created_at`, `updated_at`) VALUES
(2, 'RS. Baptis', 'Jl. Brigjend Pol. IBH Pranoto ', '(0354) 682170', 'RS Baptis Kediri adalah Rumah Sakit yang berada di kota Kediri yang berdiri sejak tahun 1957. Dimana Rumah Sakit ini dilandasi dengan pelayanan kasih terhadap sesama, yang didasari dengan iman, sebagai manifestasi rasa syukur kepada Tuhan. Perbaikan yang berkesinambungan dalam mutu pelayanan & fasilitas.merupakan bentuk konsekuensi kami sebagai \"Sahabat Terpercaya Menuju Sehat\" Visi : Menjadi Rumah Sakit Pilihan dan Rujukan Utama dengan dasar Kasih Kristus Misi : Memberikan Pelayanan Kesehatan prima secara holistik yang berlandaskan kasih Kristus kepada setiap orang tanpa membedakan status sosial, golongan, suku dan agama Memberikan pelayanan kesehatan berstandart akreditasi terbaik Value Iman, Kasih & Pengharapan', 'rs_babtis.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(3, 'RS. Gambiran', 'Jl. Kapten Tendean No.16, Paku', '(0354) 2810000', 'Rumah Sakit Umum Daerah Gambiran merupakan Rumah Sakit Milik pemerintah Daerah Kota Kediri, yang secara historis di bangun oleh bangsa Belanda pada tahun 1875 dan mulai dikembangkan pada tahun 1928 dan merupakan rumah sakit yang pertama di Daerah Karesidenan Kediri , sehingga pada waktu itu menjadi pusat rujukan kesehatan penduduk daerah – daerah sekitarnya di Karesidenan Kediri.', 'RS_GAMBIRAN.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(4, 'RS. Aura Syifa', 'Jl. Joyoboyo No 42 Dlopo, Kara', '(0354) 682316', 'RS Aura Syifa Kediri merupakan salah satu RSU dibawah naungan Yayasan Aura Nurani dan memperoleh izin operasional pada tahun 2017. Memiliki beberapa fasilitas seperti UGD, Bank Darah, Ambulans, Instalasi Bersalin, dan Laboratorium. RS ini menyediakan beberapa poliklinik seperti Poliklinik Bedah, Poliklinik Kandungan dan Kebidanan, Poliklinik Jantung dan Poliklinik Saraf.', 'RS_AURA_SYIFA.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(5, 'RS. Muhammadiyah Ahmad Dahlan', 'Jl. Gatot Subroto N0 84 Mrican', '(0354) 773115', 'Rumah Sakit Muhammadiyah Ahmad Dahlan Kota Kediri terletak ditempat yang sangat strategis yaitu di Jalan Gatot Subroto No 84 kota kediri, di desa Ngampel kecamatan Mojoroto Kota Kediri. Jalan tersebut jaan propinsi merupakan jalur masuk kota kediri dari arah barat dan utara.\r\n\r\nSesuai dengan tugas dan fungsi Rumah sakit, maka RS Muhammadiyah Ahmad Dahlan Kota Kediri memberikan layanan yang bersifat promotif, preventif, kuratif dan rehabilitatif secara komprehensif. Layanan-layanan tersebut diwujudkan dalam bentuk fasilitas-fasilitas layanan medis dan keperawatan, layanan penunjang medis, layanan administratif dan layanan pendukung lainnya.', 'RS_MUHAMADIYAH.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(6, 'RS. Tk IV DKT ', 'Jl. Mayjend Sungkono No 44, Se', '(0354) 687801', 'Sejarah berdirinya Rumkit Tk. IV Kediri terbentuk sebelum tahun 1950 yang dmulai dari terbentuknya Detasemen Kesehatan Tentara / DKT Divisi VII dan Divisi VI yang berkedudukan di Gatul Les Padangan Mojokerto pada tahun 1945-1946 yang dipimpin oleh Letnan Kolonel I. Radimin.\r\n\r\nPada bulan Januari tahun 1947 alat –alat medis sebagian dipindahkan di Kawedanan, Rumah Gadai dan di rumah pensiunan Patih. Karena situasi genting alat-alat medis dipindahkan lagi ke Mojoagung. Tepatnya bulan Maret 1947 Markas DKT diduduki oleh Belanda, sehingga DKT Divisi VI dipindahkan ke Kediri dan berubah namanya menjadi DKT Resimen 34 dengan kedudukan di jalan Veteran Kediri / tepatnya sekarang SMA Negeri I Kediri, dan terbentuk Rumah Sakit Tentara Divisi VI bertempat di jalan Mayjen Sungkono No 44 Kediri, yang kemudian ditetapkan menjadi Rumah Sakit Detasemen Kesehatan 081 Kediri . Sedangkan bekal obat-obatan dan bekal lainya ditempatkan di Asrama utara jalan Mayjen Sungkono no 48 Kediri.\r\n\r\nPada tahun 1948 Rumah Sakit Detasemen Kesehatan 081 kediri dalam perkembangan pengabdianya membangun asrama keluarga disebelah selatan, 1 Asrama Putri dan 3 gudang yang saat itu ditinjau oleh Panglima Besar Jendral Sudirman.\r\n\r\nPada bulan Desember 1948 Kediri diduduki oleh Belanda, sehingga seluruh anggota Rumah Sakit dibagi beberapa kelompok dan dibagi menjadi beberapa pos tersebar di wilayah Kediri dan Nganjuk.', 'RS_DKT.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(7, 'RS. Ratih ', 'Jl. Penanggungan, Bandar Lor, ', '(0354) 779500', 'RS Ratih adalah salah satu Layanan Kesehatan milik Organisasi Sosial Kota Kediri yang bermodel RSU, diurus oleh  Yayasan Al – Mabrur  organisasi_sosial dan termaktub kedalam Rumah Sakit Tipe Belum ditetapkan. Layanan Kesehatan ini telah terdaftar sejak  17/02/2012 dengan Nomor Surat ijin  – dan Tanggal Surat ijin  00/00/0000 dari  – dengan Sifat  perpanjangan, dan berlaku sampai  -. Sesudah mengadakan Prosedur AKREDITASI RS Seluruh Indonesia dengan proses  – akhirnya diberikan status  belum Akreditasi Rumah Sakit. RSU ini bertempat di Jl. Penanggungan No.32, Kediri, Kota Kediri, Indonesia.', 'RS_RATIH.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(8, 'RS. Bhayangkara Kediri', 'JL. KOMBES POL. DURYAT NO. 17 ', '(0354) 683 830', 'Rumah Sakit Bhayangkara Kediri berdiri pada tanggal 18 September 1971 dengan nama Balai Pengobatan/ Rumah Bersalin Kepolisian Kediri, kemudian sesuai dengan Akte Notaris nomor: 17 Tahun 1972 tanggal 26 Mei 1972 berubah status menjadi Yayasan Bhayangkara Kediri. Selanjutnya berubah lagi menjadi Rumah Sakit Bhayangkara Kowil Kepolisian 104 Kediri dan berdasarkan  Surat Keputusan Kepala Daerah Kepolisian X Jawa Timur Nomor : Skep / 232 / XII / 1982 tanggal 18 Desember 1982 ditetapkan menjadi Rumah Sakit Bhayangkara Kodak X Jawa Timur di Kediri, kemudian berubah lagi menjadi Rumah Sakit Kepolisian Tingkat III Kediri berdasarkan surat Keputusan Menteri Pertahanan/ Panglima Angkatan Bersenjata Nomor : Skep/ 246 / VI / 1985.\r\n\r\nSejak tanggal 26 Pebruari 2007 berdasarkan Surat Keputusan Menteri Kesehatan Nomor: YM. 02. 0. 3. 1072. dinamakan Rumah Sakit Bhayangkara Kediri. Pada Tahun 2010 Rumah Sakit Bhayangkara Kediri ditetapkan oleh Pemerintah menjadi Rumah Sakit PK-BLU berdasarkan Keputusan Menteri Keuangan No. 418/KMK.05/2010 tanggal 7 Oktober 2010.\r\n\r\nBerdasarkan Keputusan Menteri Kesehatan Republik Indonesia Nomor : HK.03.05/I/972/12 tanggal 14 Juni 2012 Rumah Sakit Bhayangkara ditetapkan sebagai Rumah Sakit Kelas B. Pada tanggal 18 Oktober 2013 berdasarkan Keputusan Kepala Kepolisian Negara Republik Indonesia Nomor : Kep/74/X/2013 tanggal 18 Oktober 2013 Rumah Sakit Bhayangkara Kediri ditetapkan statusnya menjadi Rumah Sakit Bhayangkara Tingkat II. ', 'RS_Bhayangkara.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(9, 'RS. Umum Lirboyo', 'Jl. Dr Saharjo, RT 01 / RW 02,', '(0354) 778165', 'RSU Lirboyo mulai berdiri sejak tahun 2006 dibawah naungan Yayasan Pondok Pesantren Lirboyo. Memiliki Visi Menjadi rumah sakit pilihan dan rujukan dengan pelayanan yang islami, professional, dan paripurna. Dengan Misi Meningkatkan mutu dan kualitas SDM, Memberikan pelayanan kesehatan paripurna, Mewujudkan persatuan untuk menciptakan kebaikan manfaat bagi Jamiiyah Nadliyin di bidang kesehatan, serta Meningkatkan derajat kesehatan santri dan masyarakat.', 'RS_LIRBOYO.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(10, 'RSUD Simpang Lima Gumul', 'Jl. Galuh Candrakirana, tugure', '(0354) 2891400', 'Pendirian UPTD RSUD Simpang Lima Gumul Kediri sebagai salah satu unsur pendukung atas penyelenggaraan Pemerintahan Daerah dibidang pelayanan kesehatan diharapkan dapat memberikan pelayanan kesehatan masyarakat yang bermutu dan paripurna \r\n\r\nvisi :\r\nMenjadi rumah sakit pilihan utama masyarakat dengan pelayanan kesehatan yang berkualitas serta mendukung pendidikan, penelitian dan pengabdian masyarakat.\r\n\r\nmisi :\r\n- Menyelenggarakan pelayanan kesehatan paripurna bagi seluruh lapisan masyarakat yang berorientasi kepada mutu dan keselamatan pasien\r\n- Membangun sumber daya manusia rumah sakit yang profesional sesuai standar diiringi integritas dalam pelayanan\r\n- Mendorong pengembangan ilmu dan teknologi kesehatan serta layanan unggulan rumah sakit\r\n- Mengembangkan pendidikan, pelatihan, penelitian dan pengabdian yang terintegrasi untuk meningkatkan mutu pelayanan\r\n- Menyelenggarakan kegiatan manajemen rumah sakit secara profesional, efektif dan efisien untuk meningkatkan kemandirian rumah sakit dan kesejahteraan karyawan\r\n\r\n\r\n', 'RS_SLG.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(11, 'RS. HVA Toeloengredjo Kediri', 'Jl. A. Yani Timur, Pare', '(0354) 391145', 'Rumah Sakit Toeloengredjo merupakan rumah sakit umum tipe C yang beralamat di jalan Ahmad Yani 25 Pare Kabupaten Kediri Jawa Timur Indonesia. Telp 0354 391145 - 391047 - 394970, Fax: 0354 - 392883 dengan alamat e-mail rstoel@yahoo.co.id dan memiliki Hot Line Service Hallo HVA dengan No telp 082230309494.\r\n\r\nRumah Sakit Toeloengredjo sudah terakreditasi PARIPURNA versi KARS 2012 pada Agustus 2016 dengan kapasitas tempat tidur pasien sebanyak 190 TT. Rumah Sakit ini memberikan beragam jenis pelayanan medis, antara lain poliklinik umum, poliklinik gigi dan mulut, poliklinik spesialis, Instalasi Gawat Darurat, Kamar operasi, Kebidananan serta rawat inap yang terdiri dari kelas I, II, III, VIP dan VVIP yang dilengkapi pelayanan laboratorium, radiologi, farmasi, fisioterapi, gizi dan laundry.', 'RS_HVA.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(12, 'RS. Siti Khodijah', 'Jl. Dr. SUtomo, No 332, Sukore', '(0354) 545481', 'Rumah Sakit Muhammadiyah Siti Khodijah Gurah merupakan unit pelayanan kesehatan di bawah Majelis Pembina Kesehatan Umum Pimpinan Daerah Muhammadiyah (MPKU-PDM) Kediri dan di bawah bimbingan Majelis Pembinaan Kesehatan Umum Pimpinan Wilayah Muhammadiyah (MPKU-PWM) Propinsi Jawa Timur, dan merupakan Institusi Pelayanan Kesehatan yang turut berperan dalam pelaksanaan system kesehatan nasional.\r\n\r\nSebagai Rumah Sakit dengan latar belakang Islami, Rumah Sakit Muhammadiyah Siti Khodijah Gurah terpanggil untuk melayani semua lapisan masyarakat tanpa memandang latar belakang suku,social dan budaya dengan perilaku Islami serta selalu berusaha meningkatkan mutu layanan untuk dapat memuaskan pelanggan.', 'RS_SITI_KHODIJAH.jpg', '', '2020-06-06 11:47:39', '0000-00-00 00:00:00'),
(16, 'RS Syuhada\' Haji', 'Blitar', '0354 XXXXXX', 'Ya gitu', 'foto.jpg', 'Soon', '2020-06-07 02:40:46', '2020-06-07 02:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spesialis`
--

CREATE TABLE `tb_spesialis` (
  `id` int(11) NOT NULL,
  `SP_NAMA` varchar(30) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_spesialis`
--

INSERT INTO `tb_spesialis` (`id`, `SP_NAMA`, `created_at`, `updated_at`) VALUES
(3, 'spesialis mata', NULL, NULL),
(4, 'spesialis kulit mu', NULL, '2020-06-05 01:06:36'),
(5, 'spesialis THT', NULL, NULL),
(6, 'spesialis bedah saraf', NULL, NULL),
(7, 'spesialis gizi', NULL, NULL),
(8, 'spesialis paru', NULL, NULL),
(9, 'spesialis radiologi', NULL, NULL),
(10, 'spesialis jiwa raga', '2020-06-06 05:54:47', '2020-06-06 05:56:51'),
(11, 'spesialis abc', '2020-06-06 10:06:07', '2020-06-06 10:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@mail.com', NULL, '$2y$10$py96dDwhmR5xfe93AGzMYOoQWZS9T5okZYvYOiswrwGSnZRfnMJYq', NULL, '2020-06-03 02:14:21', '2020-06-03 02:14:21'),
(2, 'arya', 'arya@gmail.com', NULL, '$2y$10$Mq2mRB/hDk4/BEtywanRPuPyizcIiqy0NWX7MFByrm8ulbgjgFdo6', NULL, '2020-06-03 15:35:23', '2020-06-03 15:35:23'),
(3, 'Isyana Wikrama D. T.', 'isyanawikramadt69@gmail.com', NULL, '$2y$10$g/TtrCJUVzT7T/qGlYwMDOQtW/c8QyKb9GNuu9RJoy8yq.l2..WqW', NULL, '2020-06-05 01:17:19', '2020-06-05 01:17:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `to_RS` (`RS_ID`),
  ADD KEY `to_SP` (`SP_ID`);

--
-- Indexes for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `get_dokter` (`DOKTER_ID`);

--
-- Indexes for table `tb_rs`
--
ALTER TABLE `tb_rs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_spesialis`
--
ALTER TABLE `tb_spesialis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tb_rs`
--
ALTER TABLE `tb_rs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_spesialis`
--
ALTER TABLE `tb_spesialis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  ADD CONSTRAINT `to_RS` FOREIGN KEY (`RS_ID`) REFERENCES `tb_rs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `to_SP` FOREIGN KEY (`SP_ID`) REFERENCES `tb_spesialis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_jadwal`
--
ALTER TABLE `tb_jadwal`
  ADD CONSTRAINT `get_dokter` FOREIGN KEY (`DOKTER_ID`) REFERENCES `tb_dokter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
